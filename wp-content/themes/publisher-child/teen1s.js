(function($) {
  // Handle Menu Bottom Responsive
  $(document).scroll(function() {
    let positionScroll = $(this).scrollTop();
    if (positionScroll > 200) {
      $(".menu-bottom-responsive").addClass("has-show");
    } else {
      $(".menu-bottom-responsive").removeClass("has-show");
    }
  });

  // Handle Add Corpright in Off Canvas
  $(".off-canvas-container .off-canvas-inner ").append(`
        <div class="copry-right">© 2020 - Teen1s. All Rights Reserved.</div>
    `);
  // Handle add class Theme Dark for Video Page
  $("body.page.page-id-12").addClass("theme-dark");

  // Handle add class Theme Dark for Single format Post
  $("body.single-format-video").addClass("theme-dark");

  //Handle add img icon share view
  $(
    ".col-sm-7.content-column .listing.columns-1 .listing-item:nth-child(12n) .views .bf-icon img"
  ).attr(
    "src",
    "/wp-content/themes/publisher-child/images/icon-fire-white.svg"
  );

  $(".post-tp-5-header .views .bf-icon img").attr(
    "src",
    "/wp-content/themes/publisher-child/images/icon-fire-white.svg"
  );

  $(".post-tp-4-header .views .bf-icon img").attr(
    "src",
    "/wp-content/themes/publisher-child/images/icon-fire-white.svg"
  );

  $(".theme-dark .views .bf-icon img").attr(
    "src",
    "/wp-content/themes/publisher-child/images/icon-fire-white.svg"
  );

  $(".theme-dark .single-post-share .post-share-btn.views .bf-icon img").attr(
    "src",
    "/wp-content/themes/publisher-child/images/icon-fire-white.svg"
  );

  $(
    ".col-sm-7.content-column .listing.columns-1 .listing-item:nth-child(12n) .share .bf-icon img"
  ).attr(
    "src",
    "/wp-content/themes/publisher-child/images/icon-share-white.svg"
  );

  $(".post-tp-4-header  .share .bf-icon img").attr(
    "src",
    "/wp-content/themes/publisher-child/images/icon-share-white.svg"
  );

  $(".post-tp-5-header .share .bf-icon img").attr(
    "src",
    "/wp-content/themes/publisher-child/images/icon-share-white.svg"
  );

  $(".theme-dark .share .bf-icon img").attr(
    "src",
    "/wp-content/themes/publisher-child/images/icon-share-white.svg"
  );
})(jQuery);
