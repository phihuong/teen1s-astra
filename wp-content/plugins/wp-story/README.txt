=== WP Story ===
Contributors: wpuzman
Donate link: https://wpuzman.com.tr
Tags: story, hikaye, stories, instagram, wordpress story, wordpress hikaye
Requires at least: 5.0.0
Tested up to: 5.3.2
Requires PHP: 5.6.0
Stable tag: 5.0.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Create your own custom Instagram style stories. Show them on any part of your site by adding custom links, text and images.

== Description ==

Create your own custom Instagram style stories. Show them on any part of your site by adding custom links, text and images.

**Free Version Features:**

* Display stories only 3 seconds.
* Create stories with only images.
* 1 visual style.

**Pro Version Features:**

* Display stories unlimited time.
* Create stories with video and images.
* 4 visual style.
* Navigation arrows.
* Cube effect.

See **<a href="https://wpuzman.xyz/neon/" target="_blank">Demo</a>**
You Can Find **<a href="https://wpuzman.com.tr/wp-story-premium">Pro Version Here</a>**

**Preview Plugin**
[youtube https://www.youtube.com/watch?v=RfArjdAq09g]

== Installation ==

1. Upload the entire `/wp-story` directory to the `/wp-content/plugins/` directory.
2. Activate WP Story through the 'Plugins' menu in WordPress.
3. Use **[wp-story]** shortcode in any wp-editor or Gutenberg.
4. Or insert **`<?php echo do_shortcode( '[wp-story]' ); ?>`** code to .php file where you want to display.
5. Enjoy.

== Frequently Asked Questions ==

= This plugin shows Instagram stories? =

No. In this plugin, you create your own stories in wp-admin panel.

== Screenshots ==

1. Create Story
2. Choose Stories
3. Preview Stories
4. Mobile Preview

== Changelog ==

= 1.0.8 =
* Some minor bugs fixed.
* Featured image is now required.
* Some helpers added.

= 1.0.7 =
* Some minor bugs fixed.

= 1.0.6 =
* Duration error fixed.

= 1.0.5 =
* Story saving error fixed.

= 1.0.4 =
* Added navigation arrows feature for pro version.
* Fixed cube effect feature for pro version.
* Link click error fixed.

= 1.0.3 =
* Body scrollbar is hidden when story modal is active.

= 1.0.2 =
* Close button fixed.
* "New Tab" option added.
* Story ID error fixed.

= 1.0.1 =
* Post type error fixed.

= 1.0.0 =
* Plugin released.

== Upgrade Notice ==

= 1.0.0 =
* Plugin released.