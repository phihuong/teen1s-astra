<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://wpuzman.com
 * @since      1.0.0
 *
 * @package    Wp_Story
 * @subpackage Wp_Story/admin/partials
 */
?>

<div class="wrap">
    <?php settings_errors(); ?>
	<h2><?php esc_html_e( 'Options', 'wp-story' ); ?></h2>

	<div class="premium-alert">
		<a href="https://wpuzman.com.tr/indirmeler/wp-story-premium/" target="_blank"><?php esc_html_e( 'This section available for only premium version.', 'wp-story' ); ?></a>
	</div>

	<div class="story-form" action="options.php" method="post">
		<a class="premium-link" href="https://wpuzman.com.tr/indirmeler/wp-story-premium/" target="_blank"></a>
        <?php
        settings_fields( $this->plugin_name . '-options' );
        do_settings_sections( $this->plugin_name . '-options' );
        ?>
		<table class="form-table">
			<tr>
				<th></th>
				<td><?php submit_button(); ?></td>
			</tr>
		</table>
	</div>
</div>