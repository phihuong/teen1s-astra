<?php
$stories = $this->stories;
if( empty( $stories ) ) return null;

foreach( $stories as $story ) {
    if( get_post( $story )->post_status == 'trash' ) return null;
}

echo '<div id="stories"></div>';
ob_start(); ?>
	<script>
        let body = document.body;

        function wpStory() {
            let stories = new Zuck('stories', {
                backNative: true,
                previousTap: true,
                autoFullScreen: true,
                openEffect: true,
                cubeEffect: false,
                skin: 'snapgram',
                avatars: true,
                list: false,
                localStorage: true,
                paginationArrows: false,
                stories: [
                    <?php
                    foreach( $stories as $story ) { ?>
                    {
                        id: "<?php echo esc_js( get_post( $story )->post_name ); ?>",
                        photo: "<?php echo esc_js( get_the_post_thumbnail_url( $story, 'full' ) ); ?>",
                        name: "<?php echo esc_js( get_the_title( $story ) ); ?>",
                        link: "<?php echo esc_js( get_the_permalink( $story ) ); ?>",
                        lastUpdated: "<?php echo esc_js( get_post_modified_time( $story ) ); ?>",
                        items: [
                            <?php
                            $post_stories = get_post_meta( $story, 'wp_story_items', true );

                            if( ! empty( $post_stories ) && is_array( $post_stories ) ) {
                            foreach( $post_stories as $post_story ) {
                            $i = 0; ?>
                            Zuck.buildItem(
                                '<?php echo esc_js( 'story-' . get_post( $story )->ID ) . '-' . $i; ?>', // ID
                                'photo', // Type
                                3, // Duration
                                '<?php echo esc_js( $post_story[ 'image' ] ); ?>', // Src
                                '', // Thumbnail
                                '<?php echo esc_js( $post_story[ 'link' ] ); ?>', // Link
                                '<?php echo esc_js( $post_story[ 'text' ] ); ?>', // Link text
                                false, // Time
                                '<?php echo esc_js( get_post_modified_time( 'U', false, $story ) ); ?>', // Seen,
                                '<?php echo $post_story[ 'new_tab' ] === 'on' ? esc_js( '_blank' ) : ''; ?>'
                            ),
                            <?php $i++; } } ?>
                        ]
                    },
                    <?php } ?>
                ],
                callbacks: {
                    onOpen(storyId, callback) {
                        callback();
                        body.classList.add('wp-story');
                        jQuery(window).scrollTop(0);
                    },
                    onClose(storyId, callback) {
                        callback();
                        body.classList.remove('wp-story');
                    },
                },
                'language': {
                    'unmute': '<?php esc_html_e( 'Touch to unmute', 'wp-story' ); ?>)',
                    'keyboardTip': '<?php esc_html_e( 'Press space to see next', 'wp-story' ); ?>',
                    'visitLink': '<?php esc_html_e( 'Visit link', 'wp-story' ); ?>',
                    'time': {
                        'ago': '<?php esc_html_e( 'ago', 'wp-story' ); ?>',
                        'hour': '<?php esc_html_e( 'hour', 'wp-story' ); ?>',
                        'hours': '<?php esc_html_e( 'hours', 'wp-story' ); ?>',
                        'minute': '<?php esc_html_e( 'minute', 'wp-story' ); ?>',
                        'minutes': '<?php esc_html_e( 'minutes', 'wp-story' ); ?>',
                        'fromnow': '<?php esc_html_e( 'from-now', 'wp-story' ); ?>',
                        'seconds': '<?php esc_html_e( 'seconds', 'wp-story' ); ?>',
                        'yesterday': '<?php esc_html_e( 'yesterday', 'wp-story' ); ?>',
                        'tomorrow': '<?php esc_html_e( 'tomorrow', 'wp-story' ); ?>',
                        'days': '<?php esc_html_e( 'days', 'wp-story' ); ?>'
                    }
                }
            });
        }

        wpStory();
	</script>
<?php echo $this->minify_js( ob_get_clean() ); ?>