'use strict';

jQuery(document).ready(function ($) {


//    function minmax(value, min, max) 
//    {
//        if(parseInt(value) < min || isNaN(parseInt(value))) 
//            return min; 
//        else if(parseInt(value) > max) 
//            return max; 
//        else return value;
//    }
//    

//    $.validator.addMethod('le', function(value, element, param) {
//      return this.optional(element) || value <= $(param).val();
//    }, 'Invalid value');
//    $.validator.addMethod('ge', function(value, element, param) {
//        return this.optional(element) || value >= $(param).val();
//    }, 'Invalid value');
//
//
    $("#wpw_auto_poster_reposter_set_submit").on('click', function (e) {
        var minimum_post_age = $("#minimum_post_age").val();
        var maximum_post_age = $("#maximum_post_age").val();
         
            if (maximum_post_age == 0 && minimum_post_age == 0) {
                $("#required_length_minimum").html("");
                $("#required_length").html("");
                return true;
            } 
            
            if(maximum_post_age > 0 && minimum_post_age == 0){
                
                $("#required_length_minimum").html("");
                $("#required_length").html("");
                return true;
                
                
//                $("#required_length").html("");
//                $("#required_length_minimum").html("");
//                $("#required_length_minimum").append("Minimum post age should not be 0");
//                $("#required_length_minimum").css("color", "#ff0000");
//                return false;
            }
            
            if(minimum_post_age > 0 && maximum_post_age == 0){
                $("#required_length_minimum").html("");
                $("#required_length").html("");
                return true;
            }
            
            if (minimum_post_age > 0 && maximum_post_age == 0) {
                $("#required_length_minimum").html("");
                $("#required_length").html("");
                return true;
            }
            
//            if(minimum_post_age > 0 && (maximum_post_age != 0 || maximum_post_age <= minimum_post_age ) ){
//                $("#required_length_minimum").html("");
//                $("#required_length").html("");
//                $("#required_length").append("Maximum post age should not be less than Minimum post age");
//                $("#required_length").css("color", "#ff0000");
//                return false;
//            }
            
            if(minimum_post_age > 0 && (maximum_post_age != 0 && maximum_post_age <= minimum_post_age ) ){
                $("#required_length_minimum").html("");
                $("#required_length").html("");
                $("#required_length").append("Maximum post age should not be less than Minimum post age");
                $("#required_length").css("color", "#ff0000");
                return false;
            }
            
//            if(maximum_post_age <= minimum_post_age){
//                $("#required_length_minimum").html("");
//                $("#required_length").html("");
//                $("#required_length").append("Maximum post age is more then Minimum post age");
//                $("#required_length").css("color", "#ff0000");
//                return false;
//            } 
                $("#required_length_minimum").html("");
                $("#required_length").html("");
                return true;
            
         

    });

    $(document).on('click', '.clear-date', function () {
        $('#_wpweb_select_hour').val('');
    });

    if ($('#_wpweb_select_hour').length) {

        $('#_wpweb_select_hour').datetimepicker({
            dateFormat: WpwAutoPosterAdmin.date_format,
            minDate: new Date(WpwAutoPosterAdmin.current_date),
            timeFormat: WpwAutoPosterAdmin.time_format,
            showMinute: false,
            ampm: false,
            stepMinute: 60,
            showOn: 'focus',
            stepHour: 1,
            currentText: '',
        }).attr('readonly', 'readonly');
    }

    if ($('#wpw_auto_select_hour').length) {

        $('#wpw_auto_select_hour').datetimepicker({
            dateFormat: WpwAutoPosterAdmin.date_format,
            minDate: new Date(WpwAutoPosterAdmin.current_date),
            timeFormat: WpwAutoPosterAdmin.time_format,
            showMinute: false,
            ampm: false,
            stepMinute: 60,
            stepHour: 1,
            currentText: 'Now',
            showOn: 'focus',
        });
    }

    if ($('.wpw-auto-schedule-content').length) {

        $(document).on('click', '.schedule > a', function (event) {

            event.preventDefault();
            var scheduleurl = $(this).attr('href');
            $("input[name='schedule_url']").val(scheduleurl);

            $(".wpw-auto-popup-content").show();
            $(".wpw-auto-popup-overlay").show();


        });

        $(document).on('click', '.wpw-close-button', function (event) {

            $(".wpw-auto-popup-content").hide();
            $(".wpw-auto-popup-overlay").hide();

        });

        $(document).on('click', '.done', function (event) {

            var bulk_action = $('#bulk-action-selector-top').val();
            var select_hour = $("input[name='wpw_auto_select_hour']").val();

            if (bulk_action != '' && bulk_action == 'schedule') {

                $('<input />').attr('type', 'hidden')
                        .attr('name', "bulk_select_hour")
                        .attr('value', select_hour)
                        .appendTo('#product-filter');

                $(".wpw-close-button").trigger("click");

            } else {

                var scheduleurl = $("input[name='schedule_url']").val();
                scheduleurl = scheduleurl + "&select_hour=" + select_hour;
                $(location).attr('href', scheduleurl);
            }

        });

        $(document).on('change', '#bulk-action-selector-top', function () {

            var action = $(this).val();

            if (action == 'schedule') {
                $(".wpw-auto-popup-content").show();
                $(".wpw-auto-popup-overlay").show();
            }
        });

    }

});