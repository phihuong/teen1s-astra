'use strict';

jQuery(document).ready(function ($) {

    if( $("#wpw_auto_poster_fb_type_post_method").length ) {
        $("#wpw_auto_poster_fb_type_post_method").chosen({search_contains:true});
    }
    if( $("#wpw_auto_poster_li_type_post_method").length ) {
        $("#wpw_auto_poster_li_type_post_method").chosen({search_contains:true});
    }
    if( $("#wpw_auto_poster_pin_type_post_method").length ) {
        $("#wpw_auto_poster_pin_type_post_method").chosen({search_contains:true});
    }
    if( $("#wpw_auto_poster_tw_type_post_method").length ) {
        $("#wpw_auto_poster_tw_type_post_method").chosen({search_contains:true});
    }
    if( $("#wpw_auto_poster_ins_type_post_method").length ) {
        $("#wpw_auto_poster_ins_type_post_method").chosen({search_contains:true});
    }
    if( $("#wpw_auto_poster_tb_type_post_method").length ) {
        $("#wpw_auto_poster_tb_type_post_method").chosen({search_contains:true});
    }
});