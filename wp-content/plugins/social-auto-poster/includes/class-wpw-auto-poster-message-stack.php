<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

/*
  Example usage:

  $messageStack = new messageStack();
  $messageStack->add('general', 'Error: Error 1', 'error');
  $messageStack->add('general', 'Error: Error 2', 'warning');
  if ($messageStack->size('general') > 0) echo $messageStack->output('general');
*/
class Wpw_Auto_Poster_Message_Stack {
  
    public $messageToStack, $messages;

	// class constructor
    function __construct() {
    	global $pagenow;

    	if( $pagenow != 'site-health.php' ) {
    		
	    	if( $this->should_start_session() ) {
				if( !session_id() ) { 
					session_cache_limiter(''); // fix header response issue for caching
					session_start();
				}	
			}
			
			$this->messages = array();
			
			if( !isset( $_SESSION['wpwautoposter_message_stack'] ) ) {
				$_SESSION['wpwautoposter_message_stack'] = array( 'messageToStack' => array() );
			}
			
			$this->messageToStack =& $_SESSION['wpwautoposter_message_stack']['messageToStack'];
			  
			for( $i=0, $n=sizeof( $this->messageToStack ); $i<$n; $i++ ) {
				$this->add( $this->messageToStack[$i]['class'], $this->messageToStack[$i]['text'], $this->messageToStack[$i]['type']);
			}
		}
		
		$this->messageToStack = array();
    }

    /**
	 * Retrieve the URI blacklist
	 *
	 * These are the URIs where we never start sessions
	 *
	 * @since  2.9.0
	 * @return boolean
	 */
    function should_start_session() {

		$start_session = true;

		if( ! empty( $_SERVER[ 'REQUEST_URI' ] ) ) {

			$blacklist = $this->get_blacklist();
			$uri       = ltrim( $_SERVER[ 'REQUEST_URI' ], '/' );
			$uri       = untrailingslashit( $uri );

			if( in_array( $uri, $blacklist ) ) {
				$start_session = false;
			}

			if( false !== strpos( $uri, 'feed=' ) ) {
				$start_session = false;
			}

			if( is_admin() && false !== strpos( $uri, 'wp-admin/admin-ajax.php' ) ) {
				// We do not want to start sessions in the admin unless we're processing an ajax request
				$start_session = false;
			}

			if( false !== strpos( $uri, 'wp_scrape_key' ) ) {
				// Starting sessions while saving the file editor can break the save process, so don't start
				$start_session = false;
			}

		}

		return apply_filters( 'wpw_auto_poster_start_session', $start_session );

	}

	/**
	 * Retrieve the URI blacklist
	 *
	 * These are the URIs where we never start sessions
	 *
	 * @since  2.9.0
	 * @return array
	 */
	public function get_blacklist() {

		$blacklist = apply_filters( 'edd_session_start_uri_blacklist', array(
			'feed',
			'feed/rss',
			'feed/rss2',
			'feed/rdf',
			'feed/atom',
			'comments/feed'
		) );

		// Look to see if WordPress is in a sub folder or this is a network site that uses sub folders
		$folder = str_replace( network_home_url(), '', get_site_url() );

		if( ! empty( $folder ) ) {
			foreach( $blacklist as $path ) {
				$blacklist[] = $folder . '/' . $path;
			}
		}

		return $blacklist;
	}

	// class methods
    function add( $class, $message, $type = '' ) {
      
		if ( $type == 'error' ) {
			$this->messages[] = array( 'params' => 'class="message_stack_error"', 'class' => $class, 'text' =>  '&nbsp;' . $message );
		} elseif ( $type == 'warning' ) {
			$this->messages[] = array( 'params' => 'class="message_stack_warning"', 'class' => $class, 'text' => '&nbsp;' . $message );
		} elseif ( $type == 'success' ) {
			$this->messages[] = array( 'params' => 'class="message_stack_success"', 'class' => $class, 'text' => '&nbsp;' . $message );
		} else {
			$this->messages[] = array( 'params' => 'class="message_stack_error"', 'class' => $class, 'text' => '' . $message );
		}
    }

    function add_session( $class, $message, $type = '' ) {
		$this->messageToStack[] = array( 'params' => 'class="message_stack_success"','class' => $class, 'text' => '' .$message, 'type' => $type );
    }

    function reset() {
		$this->messages = array();
    }

    function output( $class ) {
     
		$str = '';
		$output = array();
		for ( $i=0, $n=count( $this->messages ); $i<$n; $i++ ) {
			if ( $this->messages[$i]['class'] == $class ) {
				$output[] = $this->messages[$i];
			}
		}
      
		$len = count( $output );
		for ( $ii=0; $ii<$len; $ii++ ) {
			$str .= '<div ' . $output[$ii]['params'] . '>' . $output[$ii]['text'] . '</div>';
		}
    
		return $str;
    }

    function size($class) {
      
		$count = 0;

		for ( $i=0, $n=sizeof( $this->messages ); $i<$n; $i++ ) {
			if ( $this->messages[$i]['class'] == $class ) {
				$count++;
			}
		}

      return $count;
    }
}