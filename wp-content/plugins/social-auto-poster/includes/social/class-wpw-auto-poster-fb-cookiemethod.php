<?php

require_once __DIR__ . '/libraries/facebook-cookie/vendor/autoload.php';

// Exit if accessed directly
if (!defined('ABSPATH'))
    exit;

/**
 * Facebook cookie method Posting Class
 * 
 * Handles functions to get and posting to user account, pages and groups
 * 
 * @package Social Auto Poster
 * @since 3.0.7
 */

class Wpw_Auto_Poster_FacebookCookieMethod
{

	private $client;
	private $fb_dtsg;
	private $fbUserId;
	private $fbSess;
	private $proxy;

	public $message;

	public function authorizeFbUser()
	{	
		 global $wpw_auto_poster_options;

        $fb_sess_data = array();

		$myInfo = $this->myInfo();

		$user_accounts = array();

		$facebook_auth_options = !empty( $wpw_auto_poster_options['facebook_auth_options'] ) ? $wpw_auto_poster_options['facebook_auth_options'] : 'graph';

		if( empty( $myInfo['id']) )
		{
			return false;
		}

		
		// my pages load		
		$accountsList = $this->getMyPages();

		$user_accounts['auth_accounts'][$myInfo['id']] = $myInfo['name'];
		$user_accounts['details'][$myInfo['id']] = array(
					'node_type' => '',
					'cookie'	=> $this->fbSess,
					'c_user'	=> $this->fbUserId
				);
		if( !empty( $accountsList )){
			foreach($accountsList AS $accountInfo)
			{

				$user_accounts['auth_accounts'][$accountInfo['id']] = $accountInfo['name'];
				$user_accounts['details'][$accountInfo['id']] = array(
						'node_type' => 'ownpage',
						'cookie'	=> $this->fbSess,
						'c_user'	=> $this->fbUserId
					);			
			}
		}
		

		$limit = 500;
		$limit = $limit >= 0 ? $limit : 0;

		$accountsList = $this->getGroups();
		
		if( !empty( $accountsList ) ){
			foreach($accountsList AS $accountInfo)
			{
				if( $limit <= 0 )
					break;

				$user_accounts['auth_accounts'][$accountInfo['id']] = $accountInfo['name'];
				$user_accounts['details'][$accountInfo['id']] = array(
						'node_type' => 'group',
						'cookie'	=> $this->fbSess,
						'c_user'	=> $this->fbUserId
					);


				$limit--;
			}
		}

		if( !empty( $user_accounts ) ) {

			$facebook_auth_options = !empty( $wpw_auto_poster_options['facebook_auth_options'] ) ? $wpw_auto_poster_options['facebook_auth_options'] : 'graph';

            $fb_sess_data[$myInfo['id']] =  array(
                        'wpw_auto_poster_fb_user_cache' => array(
                            'name' => $myInfo['name'],
                            'id' => $myInfo['id'],
                            'driver' =>	'fb',
                            'cookie' => $this->fbSess,
                            'c_user'	=> $this->fbUserId

                            ),
                        'wpw_auto_poster_fb_user_id' => $myInfo['id'],
                        'wpw_auto_poster_fb_user_accounts' => $user_accounts,
                    );

            if( $facebook_auth_options != 'cookie') { // privious options is grapth then remove old data
                update_option('wpw_auto_poster_fb_sess_data', $fb_sess_data );
                $wpw_auto_poster_options['facebook_auth_options'] = 'cookie';

                update_option('wpw_auto_poster_options', $wpw_auto_poster_options );
                
            }
            else { // merge account facebook session data

                $wpw_auto_poster_fb_sess_data = get_option('wpw_auto_poster_fb_sess_data');

                if( !empty( $wpw_auto_poster_fb_sess_data ) ) {

                    foreach ( $wpw_auto_poster_fb_sess_data as $fb_app_id => $sees_data ) {

                        if( $fb_app_id != $sees_data['wpw_auto_poster_fb_user_id'] || !isset( $sees_data['wpw_auto_poster_fb_user_cache']['c_user'] ) ){
                            unset( $wpw_auto_poster_fb_sess_data[$fb_app_id]);
                        }
                    }
                }

                $wpw_auto_poster_fb_sess_data[$myInfo['id']] = $fb_sess_data[$myInfo['id']];

                update_option('wpw_auto_poster_fb_sess_data', $wpw_auto_poster_fb_sess_data );

            }

            $this->message->add_session('poster-selected-tab', 'facebook');

            return true;
	    }

	    return false;
	}

	public function set( $fbUserId, $fbSess, $proxy = null )
	{
		global $wpw_auto_poster_message_stack;

		$this->fbUserId	= $fbUserId;
		$this->fbSess	= $fbSess;
		$this->proxy	= $proxy;
		$this->message = $wpw_auto_poster_message_stack;
		
		$cookies = [
			["Name" => "c_user", "Value"  => $fbUserId,  "Domain" => ".facebook.com","Path" => "/","Max-Age" => null,"Expires" => null,"Secure" => false,"Discard" => false,"HttpOnly" => false,"Priority" => "HIGH"],
			["Name" => "xs","Value" => $fbSess, "Domain" => ".facebook.com","Path" => "/","Max-Age" => null,"Expires" => null,"Secure" => false,"Discard" => false,"HttpOnly" => true,"Priority" => "HIGH"]
		];

		$cookieJar		= new \GuzzleHttp\Cookie\CookieJar(false , $cookies);

		$this->client	= new \GuzzleHttp\Client([
			'cookies' 			=>	$cookieJar,
			'allow_redirects'	=>	[ 'max' => 20 ],
			'proxy'				=>	empty($proxy) ? null : $proxy,
			'verify'			=>	false,
			'http_errors'		=>	false,
			'headers'			=>	['User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:66.0) Gecko/20100101 Firefox/66.0']
		]);
	}

	public function getMyPages()
	{
		$myPagesArr = [];

		$result = (string)$this->client->request('GET' , 'https://www.facebook.com/bookmarks/pages?ref=u2u' )->getBody();

		preg_match('/require\:(\[\[\"BookmarkSeeAllEntsSectionController\".+\}\]\]\]\])\,/Ui', $result, $myPages);

		$myPages = preg_replace('/(\,|\{)([a-zA-Z0-9\_]+)\:/', '$1"$2":', $myPages[1]);
		$myPages = json_decode( $myPages, true );

		$myPages = is_array( $myPages ) && isset($myPages[0][3][1]) ? $myPages[0][3][1] : [];

		foreach( $myPages AS $myPageInf )
		{
			$myPagesArr[] = [
				'id'    	=>  isset($myPageInf['id']) ? $myPageInf['id'] : 0,
				'name'  	=>  isset($myPageInf['name']) ? $myPageInf['name'] : '-',
				'category'	=>	''
			];
		}


		return $myPagesArr;
	}

	public function getLikedPages()
	{
		$result = (string)$this->client->request('GET' , 'https://touch.facebook.com/pages/launchpoint/liked_pages/' )->getBody();

		preg_match('/\<ul.*\>(.+)\<\/ul\>/Ui', $result, $likedPagesList);

		if( ! isset( $likedPagesList[1] ) )
			return [];

		preg_match_all('/\<li.+\>(.+)\<\/li\>/Ui', $likedPagesList[1], $likedPages);

		if( ! isset( $likedPages[1] ) )
			return [];

		$likedPagesArr = [];

		foreach( $likedPages[1] AS $myPage )
		{
			preg_match( '/page_id\"\: ?([0-9]+)/i', urldecode($myPage), $pageId );
			$pageId = isset($pageId[1]) ? $pageId[1] : 0;

			preg_match( '/\<div class\=\"_4nwe\"\>(.+)\<\/div\>/Ui', $myPage, $pageName );
			$pageName = isset($pageName[1]) ? $pageName[1] : '???';

			preg_match( '/\<div class\=\"_4nw9\"\>(.+)\<span/Ui', $myPage, $pageCategory );
			$pageCategory = isset($pageCategory[1]) ? $pageCategory[1] : '???';

			$likedPagesArr[] = [
				'id'        =>  $pageId,
				'name'  =>  $pageName,
				'category'  =>  $pageCategory
			];
		}

		return $likedPagesArr;
	}

	public function getGroups()
	{
		$result = (string)$this->client->request('GET' , 'https://m.facebook.com/groups/?seemore' )->getBody();

		preg_match_all('/\<a href\=\"\/groups\/([0-9]+)(?:\?[^\"]+)?\"\>(.+)\<\/a\>/Ui', $result, $groups);

		if( ! isset( $groups[1] ) )
			return [];

		$groupsArr = [];

		foreach( $groups[1] AS $key => $group )
		{
			$groupsArr[] = [
				'id'        =>  $group,
				'name'      =>  isset($groups[2][$key]) ? $groups[2][$key] : '???'
			];
		}

		return $groupsArr;
	}

	public function getStats( $postId )
	{
		$result = (string)$this->client->request('GET' , 'https://touch.facebook.com/' . $postId )->getBody();

		preg_match('/\,comment_count\:([0-9]+)\,/i', $result, $comments);
		preg_match('/\,share_count\:([0-9]+)\,/i', $result, $shares);
		preg_match('/\,reactioncount\:([0-9]+)\,/i', $result, $likes);

		return [
			'like'      =>  isset($likes[1]) ? $likes[1] : 0,
			'comments'  =>  isset($comments[1]) ? $comments[1] : 0,
			'shares'    =>  isset($shares[1]) ? $shares[1] : 0,
			'details'   =>  ''
		];
	}

	public function sendPost( $nodeFbId, $nodeType , $type , $message , $preset_id , $link , $images = array() , $video = '' )
	{

		$sendData = [
			'fb_dtsg'   =>  $this->fb_dtsg(),
			'__ajax__'  =>  'true'
		];

		if( empty( $sendData['fb_dtsg'] ) )
		{
			return [
				'status'	=>	'error',
				'error_msg'	=>	'Session expired. Please remove your Facebook account from the plugin and add it again! P.S. Don\'t Log out from your Facebook account after adding that . When you Log out your account, session will be destroyed and you can\'t share any post. Best practic is to add your account within Incognito mode!'
			];
		}

		if( $preset_id > 0 && $type == 'status' )
		{
			$sendData['text_format_preset_id'] = $preset_id;
		}
		else if( $type == 'link' )
		{
			$sendData['linkUrl'] = wpw_auto_poster_spintax( $link );
		}

		$postType = 'form_params';

		if( $type == 'image' )
		{
			$sendData['photo_ids'] = [];

			$images = is_array($images) ? $images : [$images];
			foreach($images AS $imageURL)
			{
				$photoId = $this->uploadPhoto($imageURL, $nodeFbId, $nodeType);

				if( $photoId > 0 )
				{
					$sendData['photo_ids'][ $photoId ] = $photoId;
				}
			}

			if( $nodeType == 'group' )
			{
				$endpoint = "https://touch.facebook.com/_mupload_/composer/?target=" . $nodeFbId;

				$sendData['message'] = wpw_auto_poster_spintax( $message );
			}
			else if( $nodeType == 'ownpage' )
			{
				$endpoint = 'https://upload.facebook.com/_mupload_/composer/?target=' . $nodeFbId . '&av=' . $nodeFbId;

				$sendData['status']             = wpw_auto_poster_spintax( $message );
				$sendData['waterfall_id']       = $this->waterfallId();
				$sendData['waterfall_source']   = 'composer_pages_feed';

				$postType = 'multipart';
			}
			else if( $nodeType == 'page' )
			{
				$endpoint = 'https://upload.facebook.com/_mupload_/composer/?target=' . $nodeFbId . '&av=' . $nodeFbId;

				$sendData['message']            = wpw_auto_poster_spintax( $message );
				$sendData['waterfall_id']       = $this->waterfallId();
				$sendData['waterfall_source']   = 'composer_pages_feed';

				$postType = 'multipart';
			}
			else
			{
				$endpoint = "https://touch.facebook.com/_mupload_/composer/?target=" . $nodeFbId;

				$sendData['status']             = wpw_auto_poster_spintax( $message );
				$sendData['waterfall_id']       = $this->waterfallId();
				$sendData['waterfall_source']   = 'composer_pages_feed';
				$sendData['privacyx']           = $this->getPrivacyX();
			}

		}
		else if( $type == 'video' )
		{
			$endPoint = 'videos';
			$sendData['file_url']		= wpw_auto_poster_spintax( $video );
			$sendData['description']	= wpw_auto_poster_spintax( $message );
		}
		else
		{

			if( $nodeType == 'group' )
			{
				$endpoint = 'https://touch.facebook.com/a/group/post/add/?gid=' . $nodeFbId;
				$sendData['message'] = wpw_auto_poster_spintax( $message );
			}
			else if( $nodeType == 'ownpage' )
			{
				$endpoint = 'https://touch.facebook.com/a/home.php?av=' . $nodeFbId;
				$sendData['status'] = wpw_auto_poster_spintax( $message );
			}
			else if( $nodeType == 'page' )
			{
				$endpoint = 'https://touch.facebook.com/a/wall.php?id=' . $nodeFbId;
				$sendData['message'] = wpw_auto_poster_spintax( $message );
			}
			else
			{
				$endpoint = 'https://touch.facebook.com/a/home.php';

				$sendData['status']     = wpw_auto_poster_spintax( $message );
				$sendData['target']     = $nodeFbId;
				$sendData['privacyx']   = $this->getPrivacyX();
			}
		}

		if( $postType == 'multipart' )
		{
			$sendData = $this->conertToMultipartArray( $sendData );
		}

		$post = (string)$this->client->request(
			'POST' ,
			$endpoint ,
			[
				$postType	=> $sendData ,
				'headers'	=> [ 'Referer' => 'https://touch.facebook.com/' ]
			]
		)->getBody();

		if( $nodeType == 'page' )
		{
			$getLastPostId = (string)$this->client->request('GET' , 'https://m.facebook.com/' . $nodeFbId . '/?filter=2' )->getBody();

			preg_match('/top_level_post_id\.([0-9]+)/i', $getLastPostId, $postId);
			$postId = isset($postId[1]) ? $postId[1] : 0;
		}
		else if( $nodeType == 'account' && $type == 'image' )
		{
			$getLastPostId = (string)$this->client->request('GET' , 'https://m.facebook.com/' . $nodeFbId . '/' )->getBody();

			preg_match('/id\=\"like_([0-9]+)\"/i', $getLastPostId, $postId);
			$postId = isset($postId[1]) ? $postId[1] : 0;
		}
		else
		{
			preg_match('/story_fbid\=([0-9]+)/i', $post, $postId);
			$postId = isset($postId[1]) ? $postId[1] : 0;

			if( !$postId ) // for: group photo upload case
			{
				preg_match('/\&(?:amp\;)?id\=([0-9]+)/i', $post, $postId);
				$postId = isset($postId[1]) ? $postId[1] : 0;
			}
		}

		return [
			'status'	=>  'ok',
			'id'		=>	$postId
		];
	}

	private function uploadPhoto( $photo, $target, $targetType )
	{
		$postData = [
			[
				'name'      => 'file1',
				'contents'  => file_get_contents($photo),
				'filename'  => basename( $photo )
			]
		];

		$endpoint = 'https://upload.facebook.com/_mupload_/photo/x/saveunpublished/?thumbnail_width=80&thumbnail_height=80&waterfall_id=' . $this->waterfallId() . '&waterfall_app_name=web_m_touch&waterfall_source=composer_pages_feed&target_id=' . urlencode( $target ) . '&fb_dtsg=' . urlencode( $this->fb_dtsg() ) . '&__ajax__=true';
		if( $targetType == 'ownpage' )
		{
			$endpoint .= '&av=' . urlencode( $target );
		}

		$post = (string)$this->client->request(
			'POST' ,
			$endpoint ,
			[
				'multipart' => $postData ,
				'headers'   => [ 'Referer' => 'https://touch.facebook.com/' ]
			]
		)->getBody();

		preg_match('/\"fbid\"\:\"([0-9]+)/i', $post, $photoId);

		return isset($photoId[1]) ? $photoId[1] : 0;
	}

	private function fb_dtsg()
	{
		if( is_null( $this->fb_dtsg ) )
		{
			$getFbDtsg = (string)$this->client->request('GET' , 'https://m.facebook.com/' )->getBody();

			preg_match('/name\=\"fb_dtsg\" value\=\"(.+)\"/Ui', $getFbDtsg, $fb_dtsg);

			if( !isset($fb_dtsg[1]) )
			{
				$this->fb_dtsg = '';
				//var_dump($getFbDtsg);
				//die;
			}
			else
			{
				$this->fb_dtsg = $fb_dtsg[1];
			}
		}

		return $this->fb_dtsg;
	}

	private function conertToMultipartArray( $arr )
	{
		$newArr = [];

		foreach( $arr AS $name => $value )
		{
			if( is_array( $value ) )
			{
				foreach($value AS $name2 => $value2)
				{
					$newArr[] = [
						'name'      => $name . '[' . $name2 . ']',
						'contents'  => $value2
					];
				}
			}
			else
			{
				$newArr[] = [
					'name'      => $name,
					'contents'  => $value
				];
			}
		}

		return $newArr;
	}

	private function waterfallId()
	{
		return md5(uniqid() . rand(0,99999999) . uniqid());
	}

	private function getPrivacyX()
	{
		$url = 'https://touch.facebook.com/privacy/timeline/saved_custom_audience_selector_dialog/?fb_dtsg=' . $this->fb_dtsg();

		$getData = (string)$this->client->request('GET' , $url )->getBody();

		preg_match('/\:\"([0-9]+)\"/i', htmlspecialchars_decode( $getData ), $firstPrivacyX);

		return isset($firstPrivacyX[1]) ? $firstPrivacyX[1] : '0';
	}

	public function myInfo()
	{
		$getInfo = (string)$this->client->request('GET' , 'https://touch.facebook.com/' )->getBody();

		preg_match('/\"USER_ID\"\:\"([0-9]+)\"/i', $getInfo, $accountId);
		$accountId = isset($accountId[1]) ? $accountId[1] : '?';

		preg_match('/\"NAME\"\:\"([^\"]+)\"/i', $getInfo, $name);
		$name = json_decode( '"' . ( isset($name[1]) ? $name[1] : '?' ) . '"' );

		return [
			'id'    =>  $accountId,
			'name'  =>  $name
		];
	}

}