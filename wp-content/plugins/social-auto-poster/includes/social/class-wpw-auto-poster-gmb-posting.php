<?php

require_once __DIR__ . '/libraries/gmb/vendor/autoload.php';
require_once __DIR__ . '/libraries/gmb/lib/google/GoogleMyBusiness.php';
require_once __DIR__ . '/libraries/gmb/lib/Providers/Request.php';

// Exit if accessed directly
if (!defined('ABSPATH'))
	exit;

/**
 * Google My Business cookie method Posting Class
 * 
 * Handles functions to get and posting to user account, pages and groups
 * 
 * @package Social Auto Poster
 * @since 3.0.7
 */

class Wpw_Auto_Poster_GMB_Posting
{
    private $gmb_sid;
    private $gmb_hsid;
    private $gmb_ssid;

    public $message;

    public function __construct() {

        global $wpw_auto_poster_message_stack, $wpw_auto_poster_model,
        $wpw_auto_poster_logs;

        $this->message = $wpw_auto_poster_message_stack;
        $this->model = $wpw_auto_poster_model;
        $this->logs = $wpw_auto_poster_logs;

        //initialize the session value when data is saved in database
    }


    /**
     * Assign Google My Business User's all Data to session
     * @package Social Auto Poster
     * @since 1.0.0
     */
    public function wpw_auto_poster_gmb_posting( $post, $auto_posting_type = '' ) {
        global $wpw_auto_poster_options;
        
        $prefix = WPW_AUTO_POSTER_META_PREFIX;
        
        $res = $this->wpw_auto_poster_post_to_gmb( $post, $auto_posting_type );
        
        if( isset( $res['success'] ) && !empty( $res['success'] ) ) { //check if error should not occured and successfully tweeted

            //record logs for posting done on google my business
            $this->logs->wpw_auto_poster_add( 'Google My Business posting completed successfully.' );
            
            update_post_meta( $post->ID, $prefix . 'gmb_published_on_posts', '1' );

            // get current timestamp and update meta as published date/time
            $current_timestamp = current_time( 'timestamp' );
            update_post_meta($post->ID, $prefix . 'published_date', $current_timestamp);
            
            return true;
        }
        
        return false;

    }


    /**
     * Post To Google My Business
     * 
     * Handles to Post on Google My Business account
     * 
     * @package Social Auto Poster
     * @since 1.0.0
     */
    public function wpw_auto_poster_post_to_gmb( $post, $auto_posting_type ) {

        global $wpw_auto_poster_options, $wpw_auto_poster_reposter_options;

        // Get stored li app grant data
        $wpw_auto_poster_gmb_sess_data = get_option('wpw_auto_poster_gmb_sess_data');

        //metabox field prefix
        $prefix = WPW_AUTO_POSTER_META_PREFIX;
        
        $post_type  = $post->post_type; //post type

        //Initilize linkedin posting
        $gmb_posting     = array();

        //Initialize tags and categories
        $tags_arr = array();
        $cats_arr = array();

        // Getting all linkedin apps
        $gmb_apps = wpw_auto_poster_get_gmb_accounts_location();

        //check linkedin authorized session is true or not
        //need to do for linkedin posting code
        if( !empty( $wpw_auto_poster_gmb_sess_data ) ) {

            //posting logs data
            $posting_logs_data  = array();

            $unique = 'false';
            
            //user data
            $userdata   = get_userdata( $post->post_author );
            $first_name = $userdata->first_name; //user first name
            $last_name  = $userdata->last_name; //user last name
            
            //published status
            $ispublished    = get_post_meta( $post->ID, $prefix . 'gmb_published_on_posts', true );

            // Get all selected tags for selected post type for hashtags support
            if(isset($wpw_auto_poster_options['gmb_post_type_tags']) && !empty($wpw_auto_poster_options['gmb_post_type_tags'])) {

                $custom_post_tags = $wpw_auto_poster_options['gmb_post_type_tags'];
                if(isset($custom_post_tags[$post_type]) && !empty($custom_post_tags[$post_type])){  
                    foreach($custom_post_tags[$post_type] as $key => $tag){
                        $term_list = wp_get_post_terms( $post->ID, $tag, array("fields" => "names") );
                        foreach($term_list as $term_single) {
                            $tags_arr[] = str_replace( ' ', '' ,$term_single);
                        }
                    }
                }
            }

            // Get all selected categories for selected post type for hashcats support
            if(isset($wpw_auto_poster_options['gmb_post_type_cats']) && !empty($wpw_auto_poster_options['gmb_post_type_cats'])) {

                $custom_post_cats = $wpw_auto_poster_options['gmb_post_type_cats'];
                if(isset($custom_post_cats[$post_type]) && !empty($custom_post_cats[$post_type])){  
                    foreach($custom_post_cats[$post_type] as $key => $category){
                        $term_list = wp_get_post_terms( $post->ID, $category, array("fields" => "names") );
                        foreach($term_list as $term_single) {
                            $cats_arr[] = str_replace( ' ', '' ,$term_single);
                        }
                    }
                    
                }
            }

            //post title
            $posttitle      = $post->post_title;
            $post_content   = $post->post_content;

            $post_content   = strip_shortcodes($post_content);

            //strip html kses and tags
            $post_content = $this->model->wpw_auto_poster_stripslashes_deep($post_content);
            
            //decode html entity
            $post_content = $this->model->wpw_auto_poster_html_decode($post_content);

            //custom title from metabox
            $customtitle    = $this->model->wpw_auto_poster_stripslashes_deep($posttitle);

            
            // custom title from custom post type message

            if( !empty( $auto_posting_type ) && $auto_posting_type == 'reposter' ) {

                // global custom post msg template for reposter
                $gmb_global_custom_message_template = ( isset( $wpw_auto_poster_reposter_options["repost_gmb_global_message_template_".$post_type] ) ) ? $wpw_auto_poster_reposter_options["repost_gmb_global_message_template_".$post_type] : '';

                $gmb_global_custom_msg_options = isset( $wpw_auto_poster_reposter_options['repost_gmb_custom_msg_options'] ) ? $wpw_auto_poster_reposter_options['repost_gmb_custom_msg_options'] : '';

                // global custom msg template for reposter
                $gmb_global_template_text = ( isset( $wpw_auto_poster_reposter_options["repost_gmb_global_message_template"] ) ) ? $wpw_auto_poster_reposter_options["repost_gmb_global_message_template"] : '';
            }
            else {

                $gmb_global_custom_message_template = ( isset( $wpw_auto_poster_options["gmb_global_message_template_".$post_type] ) ) ? $wpw_auto_poster_options["gmb_global_message_template_".$post_type] : '';

                $gmb_global_custom_msg_options = isset( $wpw_auto_poster_options['gmb_custom_msg_options'] ) ? $wpw_auto_poster_options['gmb_custom_msg_options'] : '';
                
                $gmb_global_template_text = ( !empty( $wpw_auto_poster_options['gmb_global_message_template'] ) ) ? $wpw_auto_poster_options['gmb_global_message_template'] : '';

            }

            if( !empty( $customtitle ) ) {

                $customtitle = $customtitle;
            } 
            

            //custom title set use it otherwise user posttiel
            $title  = !empty( $customtitle ) ? $customtitle : $post_content;

            //post image
            $postimage      = get_post_meta( $post->ID, $prefix . 'gmb_post_image', true );

            /**************
             * Image Priority
             * If metabox image set then take from metabox
             * If metabox image is not set then take from featured image
             * If featured image is not set then take from settings page
             **************/

            //get featured image from post / page / custom post type
            $post_featured_img = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );

            // global custom post img
            $gmb_custom_post_img = ( isset( $wpw_auto_poster_options["gmb_post_image_".$post_type] ) ) ? $wpw_auto_poster_options["gmb_post_image_".$post_type] : '';

            $gmb_global_custom_msg_options = isset( $wpw_auto_poster_options['gmb_custom_msg_options'] ) ? $wpw_auto_poster_options['gmb_custom_msg_options'] : '';

            //check custom image is set in meta and not empty
            if( isset( $postimage['src'] ) && !empty( $postimage['src'] ) ) {
                $postimage = $postimage['src'];
            } elseif ( isset( $post_featured_img[0] ) && !empty( $post_featured_img[0] ) ) {
                //check post featrued image is set the use that image
                $postimage = $post_featured_img[0];
            } else {
                //else get post image from settings page
                $postimage = ( $gmb_global_custom_msg_options == 'post_msg' && !empty( $gmb_custom_post_img ) ) ? $gmb_custom_post_img : $wpw_auto_poster_options['gmb_post_image'];
            }

            $postimage = apply_filters('wpw_auto_poster_social_media_posting_image', $postimage );

            //post link
            //$postlink = get_post_meta( $post->ID, $prefix . 'gmb_custom_post_link', true );
            $postlink = get_the_permalink( $post->ID );
            $postlink = isset( $postlink ) ? $postlink : '';
            //if custom link is set or not
            $customlink = !empty( $postlink ) ? 'true' : 'false';


            //do url shortner
            $postlink = $this->model->wpw_auto_poster_get_short_post_link( $postlink, $unique, $post->ID, $customlink, 'gmb' );


            // not sure why this code here it should be above $postlink but lets keep it here
            //if post is published on linkedin once then change url to prevent duplication
            if( isset( $ispublished ) && $ispublished == '1' ) {
                $unique = 'true';
            }

            //comments
            $description = get_post_meta( $post->ID, $prefix . 'gmb_custom_status_msg', true );

            $description = !empty( $description ) ? $description : '';
            $description = apply_filters( 'wpw_auto_poster_gmb_comments', $description, $post );


            if( $gmb_global_custom_msg_options == 'post_msg' && !empty( $gmb_global_custom_message_template ) && empty( $description ) ) {

                $description = $gmb_global_custom_message_template;
            }
            elseif( empty( $description ) && !empty( $gmb_global_template_text ) ) {

                $description = $gmb_global_template_text;
            } elseif( empty( $description ) ){
                //get linkedin posting description
                $description = $posttitle;
            }

            // Get post excerpt
            $excerpt = !empty( $post->post_excerpt ) ? $post->post_excerpt : '';

            // Get post tags
            $tags_arr   = apply_filters('wpw_auto_poster_gmb_hashtags', $tags_arr);
            $hashtags   = ( !empty( $tags_arr ) ) ? '#'.implode( ' #', $tags_arr ) : '';

            // get post categories
            $cats_arr   = apply_filters('wpw_auto_poster_gmb_hashcats', $cats_arr);
            $hashcats   = ( !empty( $cats_arr ) ) ? '#'.implode( ' #', $cats_arr ) : '';

            $full_author = $first_name.' '.$last_name;
            $nickname_author = get_user_meta( $post->post_author, 'nickname', true);

            $search_arr         = array( '{title}','{link}', '{full_author}', '{nickname_author}', '{post_type}', '{first_name}' , '{last_name}', '{sitename}', '{site_name}', '{content}', '{excerpt}', '{hashtags}', '{hashcats}' );
            $replace_arr        = array( $posttitle ,$postlink, $full_author, $nickname_author, $post_type, $first_name, $last_name, get_option( 'blogname'), get_option( 'blogname' ), $post_content, $excerpt, $hashtags, $hashcats );

            $code_matches = array();

            // check if template tags contains {content-numbers}
            if( preg_match_all( '/\{(content)(-)(\d*)\}/', $description, $code_matches ) ) {
                $trim_tag = $code_matches[0][0];
                $trim_length = $code_matches[3][0];
                $post_content = substr( $post_content, 0, $trim_length);
                $search_arr[] = $trim_tag;
                $replace_arr[] = $post_content;
            }

            $cf_matches = array();

            // check if template tags contains {CF-CustomFieldName}
            if( preg_match_all( '/\{(CF)(-)(\S*)\}/', $description, $cf_matches ) ) {

                foreach ($cf_matches[0] as $key => $value)
                {
                    $cf_tag = $value;

                    $search_arr[] = $cf_tag;
                }

                foreach ($cf_matches[3] as $key => $value)
                {
                    $cf_name = $value;
                    $tag_value = '';
                    
                    if( $cf_name ) {
                        $tag_value = get_post_meta($post->ID, $cf_name, true);

                        if( is_array( $tag_value ) ) {
                            $tag_value = '';
                        }
                    }

                    $replace_arr[] = $tag_value;
                }
            }


            $description = str_replace( $search_arr, $replace_arr, $description );


            $description = $this->model->wpw_auto_poster_stripslashes_deep( $description );

            $description = $this->model->wpw_auto_poster_html_decode( $description );

            // replace title with tag support value                 
            $search_arr         = array( '{title}', '{link}','{full_author}', '{nickname_author}', '{post_type}', '{first_name}' , '{last_name}', '{sitename}', '{site_name}', '{content}', '{excerpt}', '{hashtags}', '{hashcats}' );
            $replace_arr        = array( $posttitle,$postlink, $full_author, $nickname_author, $post_type, $first_name, $last_name, get_option( 'blogname'), get_option( 'blogname' ), $post_content, $excerpt, $hashtags, $hashcats );

            // check if template tags contains {content-numbers}
            if( preg_match_all( '/\{(content)(-)(\d*)\}/', $title, $code_matches ) ) {
                $trim_tag = $code_matches[0][0];
                $trim_length = $code_matches[3][0];
                $post_content = substr( $post_content, 0, $trim_length);
                $search_arr[] = $trim_tag;
                $replace_arr[] = $post_content;
            }

            // check if template tags contains {CF-CustomFieldName}
            if( preg_match_all( '/\{(CF)(-)(\S*)\}/', $title, $cf_matches ) ) {

                foreach ($cf_matches[0] as $key => $value)
                {
                    $cf_tag = $value;

                    $search_arr[] = $cf_tag;
                }

                foreach ($cf_matches[3] as $key => $value)
                {
                    $cf_name = $value;
                    $tag_value = '';
                    
                    if( $cf_name ) {
                        $tag_value = get_post_meta($post->ID, $cf_name, true);

                        if( is_array( $tag_value ) ) {
                            $tag_value = '';
                        }
                    }

                    $replace_arr[] = $tag_value;
                }
            }

            // replace title with tag support value
            $title              = str_replace( $search_arr, $replace_arr, $title );

            //Get title
            $title              = $this->model->wpw_auto_poster_html_decode( $title );

            //use 400 character to post to linkedin will use as title
            $description    = $this->model->wpw_auto_poster_excerpt( $description, 400 );

            //Get comment
            $comments           = $this->model->wpw_auto_poster_html_decode( $description );
            $comments           = $this->model->wpw_auto_poster_excerpt( $comments, 700 );

            //Linkedin Profile Data from setting //_wpweb_li_post_profile
            $gmb_post_profiles   = get_post_meta( $post->ID, $prefix . 'gmb_user_id' );

            /******* Code to posting to selected category Google My Business account ******/

            // get all categories for custom post type
            $categories = wpw_auto_poster_get_post_categories_by_ID( $post_type, $post->ID );

            // Get all selected account list from category
            $category_selected_social_acct = get_option( 'wpw_auto_poster_category_posting_acct');

            // IF category selected and category social account data found
            if( !empty( $categories ) && !empty( $category_selected_social_acct ) && empty( $gmb_post_profiles ) ) {
                $gmb_clear_cnt = true;

                // GET Linkdin user account ids from post selected categories
                foreach ( $categories as $key => $term_id ) {

                    $cat_id = $term_id;
                    // Get TW user account ids form selected category  
                    if( isset( $category_selected_social_acct[$cat_id]['gmb'] ) && !empty( $category_selected_social_acct[$cat_id]['gmb'] ) ) {
                        // clear TW user data once
                        if( $gmb_clear_cnt)
                            $gmb_post_profiles = array();
                        $gmb_post_profiles = array_merge($gmb_post_profiles, $category_selected_social_acct[$cat_id]['gmb'] );
                        $gmb_clear_cnt = false;
                    }
                }
                if( !empty( $gmb_post_profiles ) ) {
                    $gmb_post_profiles = array_unique($gmb_post_profiles);
                }
            }


            if( empty( $gmb_post_profiles ) ) {//If profiles are empty in metabox
                $gmb_post_profiles   = isset( $wpw_auto_poster_options['gmb_type_'.$post->post_type.'_user'] ) ? $wpw_auto_poster_options['gmb_type_'.$post->post_type.'_user'] : '';
            }

            $content = array( 
                'title'                 => $title,
                'submitted-url'         => $postlink,
                'comment'               => $comments,
                'submitted-image-url'   => $postimage,
                'description'           => $description
            );

            //posting logs data
            $posting_logs_data = array( 
                'title'         => $title,
                'link'          => $postlink,
                'image'         => $postimage,
                'description'   => $description
            );
            /*echo '<pre>';
            print_r($wpw_auto_poster_gmb_sess_data); exit;*/
            $this->logs->wpw_auto_poster_add( 'Google My Business post data : ' . var_export( $content, true ) );
            
            //initial value of posting flag
            $postflg = false;
            try {

                if( !empty( $gmb_post_profiles ) ) {

                    foreach ( $gmb_post_profiles as $gmb_post_profile ) {
                        //Initilize log user details
                        $posting_logs_user_details  = array();
                        $proxy = '';
                        $gmb_users_id = explode("|",$gmb_post_profile);
                        
                        if( in_array( $gmb_users_id[0] , $wpw_auto_poster_gmb_sess_data[$gmb_users_id[1]]['wpw_auto_poster_gmb_user_accounts'][$gmb_users_id[1]] ) ){
                            $gmb_sid    =   $wpw_auto_poster_gmb_sess_data[$gmb_users_id[1]]['wpw_auto_poster_gmb_user_accounts'][$gmb_users_id[1]]['gmb_sid'];
                            $gmb_hsid    =   $wpw_auto_poster_gmb_sess_data[$gmb_users_id[1]]['wpw_auto_poster_gmb_user_accounts'][$gmb_users_id[1]]['gmb_hsid'];
                            $gmb_ssid    =   $wpw_auto_poster_gmb_sess_data[$gmb_users_id[1]]['wpw_auto_poster_gmb_user_accounts'][$gmb_users_id[1]]['gmb_ssid'];


                            $posting_logs_user_details['display_name']  = $wpw_auto_poster_gmb_sess_data[$gmb_users_id[1]]['wpw_auto_poster_gmb_user_cache']['name'];
                            $posting_logs_user_details['id']            = $wpw_auto_poster_gmb_sess_data[$gmb_users_id[1]]['wpw_auto_poster_gmb_user_cache']['id'];
                            $posting_logs_user_details['gender']        = $wpw_auto_poster_gmb_sess_data[$gmb_users_id[1]]['wpw_auto_poster_gmb_user_cache']['gender'];
                            $posting_logs_user_details['profile_img']     = $wpw_auto_poster_gmb_sess_data[$gmb_users_id[1]]['wpw_auto_poster_gmb_user_cache']['profile_image'];

                            if( isset($gmb_sid) && !empty($gmb_sid) && isset($gmb_hsid) && !empty($gmb_hsid) && isset($gmb_ssid) && !empty($gmb_ssid)  ) {

                                $proxy = '';
                                $google = new GoogleMyBusiness( $gmb_sid , $gmb_hsid , $gmb_ssid , $proxy );

                                if(!$google){

                                    $this->logs->wpw_auto_poster_add('Google My Business error: Google My Business is not initialized with '.$gmb_users_id[0].' account.'); // Record logs for google my business not initialized
                                    continue;
                                }
                                $link_button_text = 'LEARN_MORE';
                                if( !empty($gmb_users_id) ){
                                    if( isset($gmb_users_id[0]) && $gmb_users_id[0] != '' ){

                                        $response = $google->sendPost($gmb_users_id[0],$content['description'], $content['submitted-url'], $link_button_text, $content['submitted-image-url']);
                                    }
                                }

                                if( !empty( $response ) && $response['status'] == 'ok' && $response['id'] != '' ) {

                                    //posting logs store into database
                                    $this->model->wpw_auto_poster_insert_posting_log( $post->ID, 'gmb', $posting_logs_data, $posting_logs_user_details );
                                    $postflg    = true;
                                    $gmb_posting['success'] = 1;

                                } else {
                                    $postflg    = false;
                                    $gmb_posting['fail'] = 0;
                                    $this->logs->wpw_auto_poster_add( 'Google My Business error: ' . $response['error_msg'] );
                // display error notice on post page
                                    sap_add_notice( sprintf( esc_html__('Google My Business: Something was wrong while posting %s', 'wpwautoposter' ), $response['error_msg'] ), 'error');
                                }
                            }
                        }
                    }
                }
            }catch( Exception $e ){
                $this->logs->wpw_auto_poster_add( 'Google My Business error: ' . $e->__toString() );
                // display error notice on post page
                sap_add_notice( sprintf( esc_html__('Google My Business: Something was wrong while posting %s', 'wpwautoposter' ), $e->__toString() ), 'error');
                return false;
            }

        }else{
            //record logs when grant extended permission not set
            $this->logs->wpw_auto_poster_add( 'Google My Business error.' );
            // display error notice on post page
            sap_add_notice( esc_html__('Google My Business: Please select location before posting to the Google My Business.', 'wpwautoposter' ), 'error');
        }
        return $gmb_posting;

    }


    /**
     * Reset Sessions
     *
     * Resetting the Google My Business sessions when the admin clicks on
     * its link within the settings page.
     *
     * @package Social Auto Poster
     * @since 1.0.0
     */
    public function wpw_auto_poster_gmb_reset_session() {

        // Check if google my business reset user link is clicked and gmb_reset_user is set to 1 and google my business user id is there
        if (isset($_GET['gmb_reset_user']) && $_GET['gmb_reset_user'] == '1' && !empty($_GET['wpw_gmb_userid'])) {

            $wpw_gmb_app_id = $_GET['wpw_gmb_userid'];

            // Getting stored li app data
            $wpw_auto_poster_gmb_sess_data = get_option('wpw_auto_poster_gmb_sess_data');

            // Unset particular app value data and update the option
            if (isset($wpw_auto_poster_gmb_sess_data[$wpw_gmb_app_id])) {
                unset($wpw_auto_poster_gmb_sess_data[$wpw_gmb_app_id]);
                update_option('wpw_auto_poster_gmb_sess_data', $wpw_auto_poster_gmb_sess_data);
            }

        }

        /******* Code for selected category Linkdin account ******/

        // unset selected Google My Business account option for category 
        $cat_selected_social_acc    = array();
        $cat_selected_acc       = get_option( 'wpw_auto_poster_category_posting_acct');
        $cat_selected_social_acc    = ( !empty( $cat_selected_acc) ) ? $cat_selected_acc : $cat_selected_social_acc;

        if( !empty( $cat_selected_social_acc ) ) {
            foreach ( $cat_selected_social_acc as $cat_id => $cat_social_acc ) {
                if( isset( $cat_social_acc['gmb'] ) ) {
                    unset( $cat_selected_acc[ $cat_id ]['gmb'] );
                }
            }
            // Update autoposter category GMB posting account options
            update_option( 'wpw_auto_poster_category_posting_acct', $cat_selected_acc );    
        }
    }

    /**
     * CheckUser For Google My Business
     * @package Social Auto Poster
     * @since 1.0.0
     */
    public function gmbcheckUser( $data )
    {
        global $wpw_auto_poster_options,$wpw_auto_poster_message_stack;

        $wpw_auto_poster_gmb_sess_data = get_option( 'wpw_auto_poster_gmb_sess_data' );

        $gmb_sess_data = array();

        $user_accounts = array();

        if( !empty($data) ){
            $proxy = '';
            $google = new GoogleMyBusiness( $data['gmb_sid'] , $data['gmb_hsid'] , $data['gmb_ssid'] , $proxy);

            try{
                $gmbUserDetails  = $google->getUserInfo();
                $Userlocation    = $google->getMyLocations();

                $user_accounts['auth_accounts'][$gmbUserDetails['id']] = $gmbUserDetails['id'];
                $user_accounts['details'][$gmbUserDetails['id']] = array(
                    'gmb_sid'   => $data['gmb_sid'],
                    'gmb_hsid'  => $data['gmb_hsid'],
                    'gmb_ssid'  => $data['gmb_ssid'],
                );

                if( !empty($Userlocation) ){
                    foreach ($Userlocation as $key => $value) {
                        $user_accounts[$gmbUserDetails['id']] =  array(
                            'id'        => $value['id'],
                            'name'      => $value['name'],
                            'category'  => $value['category'],
                            'gmb_sid'   => $data['gmb_sid'],
                            'gmb_hsid'  => $data['gmb_hsid'],
                            'gmb_ssid'  => $data['gmb_ssid'],
                        );
                    }
                }

                if( !empty( $gmbUserDetails )){
                    $gmb_sess_data[$gmbUserDetails['id']] =  array(
                        'wpw_auto_poster_gmb_user_cache' => array(
                            'name'      => $gmbUserDetails['name'],
                            'id'        => $gmbUserDetails['id'],
                            'driver'    => 'gmb',
                            'gender'    => $gmbUserDetails['gender'],
                            'profile_image'  => $gmbUserDetails['profile_image'],
                        ),
                        'wpw_auto_poster_gmb_user_id' => $gmbUserDetails['id'],
                        'wpw_auto_poster_gmb_user_accounts' => $user_accounts,
                    );

                    if( !empty($wpw_auto_poster_gmb_sess_data) ){
                        $gmb_sess_data = array_merge( $wpw_auto_poster_gmb_sess_data , $gmb_sess_data );
                    }else{
                        $gmb_sess_data = $gmb_sess_data;
                    }
                    update_option('wpw_auto_poster_gmb_sess_data', $gmb_sess_data );
                    
                }

                $wpw_auto_poster_message_stack->add_session('poster-selected-tab', 'googlemybusiness');
                return true;
            }catch(Exception $e){
                $this->logs->wpw_auto_poster_add( 'Google My Business error: ' . $e->__toString() );
                // display error notice on post page
                return false;
            }
            return false;
        }
    }
}