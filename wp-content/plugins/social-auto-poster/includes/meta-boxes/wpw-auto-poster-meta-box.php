<?php

// Exit if accessed directly
if (!defined('ABSPATH'))
    exit;

/**
 * Plugin Loaded
 * 
 * Add metabox fields in plugin loaded action.
 * 
 * @package Social Auto Poster
 * @since 1.6.2
 */
function wpw_auto_poster_add_meta_boxes() {

    //include extended metabox class to user in poster plugin

    require_once( WPW_AUTO_POSTER_META_DIR . '/class-wpw-auto-poster-meta.php' );


    global $wpw_auto_poster_model, $wpw_auto_poster_options, $wpw_auto_poster_fb_posting, $wpw_auto_poster_tw_posting, $wpw_auto_poster_li_posting, $wpw_auto_poster_tb_posting, $wpw_auto_poster_ba_posting,$wpw_auto_poster_yt_posting,$wpw_auto_poster_gmb_postings;

    // Facebook app version
    $fb_app_version = ( !empty( $wpw_auto_poster_options['fb_app_version'] ) ) ? $wpw_auto_poster_options['fb_app_version'] : '';

    $facebook_auth_options = !empty( $wpw_auto_poster_options['facebook_auth_options'] ) ? $wpw_auto_poster_options['facebook_auth_options'] : 'graph';

    //model class
    $model = $wpw_auto_poster_model;

    //posting class
    $fbposting = $wpw_auto_poster_fb_posting;
    $twposting = $wpw_auto_poster_tw_posting;
    $liposting = $wpw_auto_poster_li_posting;
    $tbposting = $wpw_auto_poster_tb_posting;
    $baposting = $wpw_auto_poster_ba_posting;
    $ytposting = $wpw_auto_poster_yt_posting;
    $gmbposting = $wpw_auto_poster_gmb_postings;

    /*
     * prefix of meta keys, optional
     * use underscore (_) at the beginning to make keys hidden, for example $prefix = '_ba_';
     *  you also can make prefix empty to disable it
     */
    $prefix = WPW_AUTO_POSTER_META_PREFIX;

    /*
     * configure your meta box
     */
    $config1 = array(
        'id' => 'wpw_auto_poster_meta', // meta box id, unique per meta box
        'title' => esc_html__('Social Auto Poster Settings', 'wpwautoposter'), // meta box title
        'pages' => 'all', //insert meta in custom post type
        'context' => 'normal', // where the meta box appear: normal (default), advanced, side; optional
        'priority' => 'high', // order of meta box: high (default), low; optional
        'fields' => array(), // list of meta fields (can be added by field arrays)
        'local_images' => false, // Use local or hosted images (meta box images for add/remove)
    );

    $poster_meta = new Wpw_Auto_Poster_Social_Meta_Box($config1);

    /*     * *********************************** Facebook Tab Starts ***************************************************** */
    $defaulttabon = true; //Active first tab by default

    //Check Post status
    $post_id = !empty($_GET['post']) ? $_GET['post'] :'';
    //Check Shedule general options
    $schedule_option = !empty( $wpw_auto_poster_options['schedule_wallpost_option'] )? $wpw_auto_poster_options['schedule_wallpost_option'] : '';
    $post_status = $post_desc = '';

    if (!isset($wpw_auto_poster_options['prevent_post_metabox']) || empty($wpw_auto_poster_options['prevent_post_metabox'])) { //check if not allowed for individual post in settings page
        $fbmetatab = array(
            'class' => 'wpw_facebook', //unique class name of each tabs
            'title' => esc_html__('Facebook', 'wpwautoposter'), //  title of tab
            'active' => $defaulttabon //it will by default make tab active on page load
        );

        $defaulttabon = false; //when facebook is on then inactive other tab by default
        //initiate tabs in metabox
        $poster_meta->addTabs($fbmetatab);

        // Get stored fb app grant data
        $wpw_auto_poster_fb_sess_data = get_option('wpw_auto_poster_fb_sess_data');

        // Get all facebook account authenticated
        $fb_users = wpw_auto_poster_get_fb_accounts('all_accounts');

        // Check facebook application id and secret must entered in settings page or not
        if ( ( WPW_AUTO_POSTER_FB_APP_ID == '' || WPW_AUTO_POSTER_FB_APP_SECRET == '' ) && $facebook_auth_options == 'graph' ) {

            $poster_meta->addGrantPermission($prefix . 'fb_warning', array('desc' => esc_html__('Enter your Facebook APP ID / Secret within the Settings Page, otherwise the Facebook posting won\'t work.', 'wpwautoposter'), 'url' => add_query_arg(array('page' => 'wpw-auto-poster-settings'), admin_url('admin.php')), 'urltext' => esc_html__('Go to the Settings Page', 'wpwautoposter'), 'tab' => 'wpw_facebook'));
        } elseif (empty($wpw_auto_poster_fb_sess_data)) { // Check facebook user id is set or not
            $poster_meta->addGrantPermission($prefix . 'fb_grant', array('desc' => esc_html__('Your App doesn\'t have enough permissions to publish on Facebook.', 'wpwautoposter'), 'url' => add_query_arg(array('page' => 'wpw-auto-poster-settings'), admin_url('admin.php')), 'urltext' => esc_html__('Go to the Settings Page', 'wpwautoposter'), 'tab' => 'wpw_facebook'));
        }

        //add label to show status
        $poster_meta->addTweetStatus($prefix . 'fb_published_on_fb', array('name' => esc_html__('Status:', 'wpwautoposter'), 'desc' => esc_html__('Status of Facebook wall post like published/unpublished/scheduled.', 'wpwautoposter'), 'tab' => 'wpw_facebook'));

        $post_status = get_post_meta($post_id, $prefix.'fb_published_on_fb', true );
        $post_label  = esc_html__('Publish Post On Facebook:', 'wpwautoposter');
        $post_desc   = esc_html__('Publish this Post to Facebook Userwall.', 'wpwautoposter');

        if( $post_status == 1 && empty($schedule_option)) {
            $post_label = esc_html__('Re-publish Post On Facebook:', 'wpwautoposter');
            $post_desc  = esc_html__('Re-publish this Post to Facebook Userwall.', 'wpwautoposter');
        } elseif ( ( $post_status == 2 ) || ( $post_status == 1 && !empty($schedule_option) ) ) {
            $post_label = esc_html__('Re-schedule Post On Facebook:', 'wpwautoposter');
            $post_desc  = esc_html__('Re-schedule this Post to Facebook Userwall.', 'wpwautoposter');
        } elseif ( empty($post_status) && !empty($schedule_option) ) {
            $post_label  = esc_html__('Schedule Post On Facebook:', 'wpwautoposter');
            $post_desc   = esc_html__('Schedule this Post to Facebook Userwall.', 'wpwautoposter');
        }

        $post_desc .= '<br>'.sprintf( esc_html__( 'If you have enabled %sEnable auto posting to Facebook%s in global settings then you do not need to check this box to publish/schedule the post. This setting is only for republishing Or rescheduling post to Facebook.', 'wpwautoposter'), '<strong>', '</strong>');
        $post_desc .= '<br><p classs="wpw-auto-poster-meta wpw-auto-poster-meta_second"><strong>'.esc_html__('Note:', 'wpwautoposter').'</strong> '. sprintf( esc_html__( 'This setting is just an event to republish/reschedule the content, It will not save any value to %sdatabase%s.', 'wpwautoposter'), '<strong>','</strong>').'</p>';

        //post to facebook
        $poster_meta->addPublishBox($prefix . 'post_to_facebook', array('name' => $post_label, 'desc' => $post_desc, 'tab' => 'wpw_facebook'));

        //Immediate post to facebook
        if( !empty($schedule_option)) {
            $poster_meta->addPublishBox($prefix . 'immediate_post_to_facebook', array('name' => esc_html__('Immediate Posting On Facebook:', 'wpwautoposter'), 'desc' => 'Immediately publish this post to Facebook Userwall.', 'tab' => 'wpw_facebook'));
        }
        
        //publish with diffrent post title
        $poster_meta->addTextarea($prefix . 'fb_custom_title', array('validate_func' => 'escape_html', 'name' => esc_html__('Custom Message:', 'wpwautoposter'), 'desc' => esc_html__('Here you can enter a custom message which will be used for the wall post. Leave it empty to use the post title. You can use following template tags within the message:', 'wpwautoposter') .
            '<br /><code>{first_name}</code> - ' . esc_html__('displays the first name,', 'wpwautoposter') .
            '<br /><code>{last_name}</code> - ' . esc_html__('displays the last name,', 'wpwautoposter') .
            '<br /><code>{title}</code> - ' . esc_html__('displays the default post title,', 'wpwautoposter') .
            '<br /><code>{link}</code> - ' . esc_html__('displays the default post link,', 'wpwautoposter') .
            '<br /><code>{full_author}</code> - ' . esc_html__('displays the full author name,', 'wpwautoposter') .
            '<br /><code>{nickname_author}</code> - ' . esc_html__('displays the nickname of author,', 'wpwautoposter') .
            '<br /><code>{post_type}</code> - ' . esc_html__('displays the post type,', 'wpwautoposter') .
            '<br /><code>{sitename}</code> - ' . esc_html__('displays the name of your site,', 'wpwautoposter') .
            '<br /><code>{excerpt}</code> - ' . esc_html__('displays the post excerpt.', 'wpwautoposter').
            '<br /><code>{hashtags}</code> - ' . esc_html__('displays the post tags as hashtags.', 'wpwautoposter').
            '<br /><code>{hashcats}</code> - ' . esc_html__('displays the post categories as hashtags.', 'wpwautoposter').
            '<br /><code>{content}</code> - ' . esc_html__('displays the post content.', 'wpwautoposter').
            '<br /><code>{content-digits}</code> - ' . sprintf(
                esc_html__('displays the post content with define number of digits in template tag. %s E.g. If you add template like {content-100} then it will display first 100 characters from post content. %s', 'wpwautoposter'),
                "<b>", "</b>"
            ).
            '<br /><code>{CF-CustomFieldName}</code> - ' . sprintf(
                esc_html__('inserts the contents of the custom field with the specified name. %s E.g. If your price is stored in the custom field "PRDPRICE" you will need to use {CF-PRDPRICE} tag. %s', 'wpwautoposter'),
                "<b>", "</b>"
            )
            , 'tab' => 'wpw_facebook', 'rows' => 3));

        do_action('wpw_auto_poster_after_custom_message_field_fb', $poster_meta, $post_id);


        //post to this account
        $poster_meta->addSelect($prefix . 'fb_user_id', $fb_users, array('name' => esc_html__('Post To This Facebook Account', 'wpwautoposter') . '(' . esc_html__('s', 'wpwautoposter') . '):', 'std' => array(''), 'desc' => esc_html__('Select an account to which you want to post. This setting overrides the global and category settings. Leave it  empty to use the global/category defaults.', 'wpwautoposter'), 'multiple' => true, 'placeholder' => esc_html__('Default', 'wpwautoposter'), 'tab' => 'wpw_facebook'));

        $wall_post_methods = array(
            '' => esc_html__('Default', 'wpwautoposter')
        );
        $wall_post_methods = array_merge($wall_post_methods, $model->wpw_auto_poster_get_fb_posting_method());

        //post on wall as a type
        $poster_meta->addSelect($prefix . 'fb_posting_method', $wall_post_methods, array('name' => esc_html__('Post As:', 'wpwautoposter'), 'std' => array(''), 'desc' => esc_html__('Select a Facebook post type. Leave it empty to use the default one from the settings page.', 'wpwautoposter'), 'tab' => 'wpw_facebook'));
        
        $share_posting_type_methods = array(
            ''              => esc_html__('Default', 'wpwautoposter'),
            'link_posting'  => esc_html__('Link posting', 'wpwautoposter'),
            'image_posting'  => esc_html__('Image posting', 'wpwautoposter'),
        );

        // Fb Share posting type as
        $poster_meta->addSelect($prefix . 'fb_share_posting_type', $share_posting_type_methods, array('name' => esc_html__('Share type:', 'wpwautoposter'), 'std' => array(''), 'desc' => esc_html__('Select a Facebook share post type. Leave it empty to use the default one from the settings page.', 'wpwautoposter'), 'tab' => 'wpw_facebook'));

        $sharepost_desc = '<br><p class="wpw-auto-poster-meta wpw-auto-poster-meta_second"><strong>'.esc_html__('Note:', 'wpwautoposter').'</strong> '. sprintf( esc_html__( 'If you are using image posting then the supported image formats are %sJPEG, BMP, PNG, GIF%s.', 'wpwautoposter'), '<strong>','</strong>').'</p>';
        $sharepost_desc .= '<p class="wpw-auto-poster-meta wpw-auto-poster-meta_second">'.sprintf( esc_html__( 'Recommend uploading image under 1MB.', 'wpwautoposter'), '<strong>','</strong>').'</p>';
        $poster_meta->addGallery($prefix . 'fb_post_gallery', array('name' => esc_html__('Image(s) to use:', 'wpwautoposter'), 'desc' => esc_html__('Here you can upload multiple images which will be used for the Facebook image posting. Leave it empty to use the featured image. if featured image is not set then it will take default image from the settings page.', 'wpwautoposter').$sharepost_desc, 'tab' => 'wpw_facebook', 'show_path' => true));

        // Display custom image if fb version below 2.9
        if( $fb_app_version < 209 ) {

            //post image url
            $poster_meta->addImage($prefix . 'fb_post_image', array('name' => esc_html__('Post Image:', 'wpwautoposter'), 'desc' => esc_html__('Here you can upload a default image which will be used for the Facebook wall post. Leave it empty to use the featured image. if featured image is also blank, then it will take default image from the settings page.', 'wpwautoposter').'<br><br><strong>'.esc_html__('Note:', 'wpwautoposter').' </strong>'.esc_html__('This option only work if your facebook app version is below 2.9. If you\'re using latest facebook app, it wont work.','wpwautoposter').' <a href="https://developers.facebook.com/blog/post/2017/06/27/API-Change-Log-Modifying-Link-Previews/" target="_blank">'.esc_html__('Learn More.', 'wpwautoposter').'</a>', 'tab' => 'wpw_facebook', 'show_path' => true));
        }

        //publish with diffrent post title
        $poster_meta->addText($prefix . 'fb_custom_status_msg', array('default' => esc_html__('New blog post:', 'wpwautoposter') . ' {title} - {link}', 'validate_func' => 'escape_html', 'name' => esc_html__('Status Update Text:', 'wpwautoposter'), 'desc' => esc_html__('Here you can enter a custom status update text. Leave it empty to  use the default one from the settings page. You can use following template tags within the status text:', 'wpwautoposter') .
            '<br /><code>{first_name}</code> - ' . esc_html__('displays the first name,', 'wpwautoposter') .
            '<br /><code>{last_name}</code> - ' . esc_html__('displays the last name,', 'wpwautoposter') .
            '<br /><code>{title}</code> - ' . esc_html__('displays the post title,', 'wpwautoposter') .
            '<br /><code>{link}</code> - ' . esc_html__('displays the post link,', 'wpwautoposter') .
            '<br /><code>{full_author}</code> - ' . esc_html__('displays the full author name,', 'wpwautoposter') .
            '<br /><code>{nickname_author}</code> - ' . esc_html__('displays the nickname of author,', 'wpwautoposter') .
            '<br /><code>{post_type}</code> - ' . esc_html__('displays the post type,', 'wpwautoposter') .
            '<br /><code>{sitename}</code> - ' . esc_html__('displays the name of your site.', 'wpwautoposter').'<br /><code>{hashtags}</code> - ' . esc_html__('displays the post tags as hashtags.', 'wpwautoposter').
            '<br /><code>{hashcats}</code> - ' . esc_html__('displays the post categories as hashtags.', 'wpwautoposter').
            '<br /><code>{content}</code> - ' . esc_html__('displays the post content.', 'wpwautoposter').
            '<br /><code>{content-digits}</code> - ' . sprintf(
                esc_html__('displays the post content with define number of digits in template tag. %s E.g. If you add template like {content-100} then it will display first 100 characters from post content.%s', 'wpwautoposter'),
                "<b>", "</b>"
            ).
            '<br /><code>{CF-CustomFieldName}</code> - ' . sprintf(
                esc_html__('inserts the contents of the custom field with the specified name. %s E.g. If your price is stored in the custom field "PRDPRICE" you will need to use {CF-PRDPRICE} tag.%s', 'wpwautoposter'),
                "<b>", "</b>"
            )
            , 'tab' => 'wpw_facebook'));
        
        // Display custom post link and description if fb version below 2.11
        if( $fb_app_version < 209 ) {

            //custom link to post to facebook
            $poster_meta->addText($prefix . 'fb_custom_post_link', array('validate_func' => 'escape_html', 'name' => esc_html__('Custom Link:', 'wpwautoposter'), 'desc' => esc_html__('Here you can enter a custom link which will be used for  the wall post. Leave it empty to use the link of the current post. The link must start with', 'wpwautoposter') . ' http://', 'tab' => 'wpw_facebook'));
        }
    }
    /*     * *********************************** Facebook Tab Ends ***************************************************** */

    /*     * *********************************** Twitter Tab Starts ***************************************************** */
    if (!isset($wpw_auto_poster_options['prevent_post_tw_metabox']) || empty($wpw_auto_poster_options['prevent_post_tw_metabox'])) { //check if not allowed for individual post in settings page
        $opttemplate = isset($wpw_auto_poster_options['tw_tweet_template']) ? $wpw_auto_poster_options['tw_tweet_template'] : 'title_link';

        $post_type = !empty( $_GET['post_type'] ) ? $_GET['post_type'] : get_post_type( $post_id ) ;

        //tweet default tempalte 
        $defaulttemplate = $model->wpw_auto_poster_get_tweet_template($opttemplate, $post_type);

        $twmetatab = array(
            'class' => 'wpw_twitter', //unique class name of each tabs
            'title' => esc_html__('Twitter', 'wpwautoposter'), //  title of tab
            'active' => $defaulttabon //it will by default make tab active on page load
        );

        $defaulttabon = false; //when twitter is on then inactive other tab by default
        //initiate tabs in metabox
        $poster_meta->addTabs($twmetatab);

        if (WPW_AUTO_POSTER_TW_CONS_KEY == '' || WPW_AUTO_POSTER_TW_CONS_SECRET == '' || WPW_AUTO_POSTER_TW_AUTH_TOKEN == '' || WPW_AUTO_POSTER_TW_AUTH_SECRET == '') {

            $poster_meta->addGrantPermission($prefix . 'tw_warning', array('desc' => esc_html__('Enter your Twitter Application Details within the Settings Page, otherwise posting to Twitter won\'t work.', 'wpwautoposter'), 'url' => add_query_arg(array('page' => 'wpw-auto-poster-settings'), admin_url('admin.php')), 'urltext' => esc_html__('Go to the Settings Page', 'wpwautoposter'), 'tab' => 'wpw_twitter'));
        }

        //Get twitter account details
        $tw_users = get_option('wpw_auto_poster_tw_account_details', array());

        //add label to show status
        $poster_meta->addTweetStatus($prefix . 'tw_status', array('name' => esc_html__('Status:', 'wpwautoposter'), 'desc' => esc_html__('Status of Twitter wall post like published/unpublished/scheduled.', 'wpwautoposter'), 'tab' => 'wpw_twitter'));

        $post_status = get_post_meta($post_id, $prefix.'tw_status', true );

        $post_label  = esc_html__('Publish Post On Twitter:', 'wpwautoposter');
        $post_desc   = esc_html__('Publish this Post to Twitter.', 'wpwautoposter');

        if( $post_status == 1 && empty($schedule_option)) {
            $post_label = esc_html__('Re-publish Post On Twitter:', 'wpwautoposter');
            $post_desc  = esc_html__('Re-publish this Post to Twitter.', 'wpwautoposter');
        } elseif ( ( $post_status == 2 ) || ( $post_status == 1 && !empty($schedule_option) ) ) {
            $post_label = esc_html__('Re-schedule Post On Twitter:', 'wpwautoposter');
            $post_desc  = esc_html__('Re-schedule this Post to Twitter.', 'wpwautoposter');
        } elseif ( empty($post_status) && !empty($schedule_option) ) {
            $post_label  = esc_html__('Schedule Post On Twitter:', 'wpwautoposter');
            $post_desc   = esc_html__('Schedule this Post to Twitter.', 'wpwautoposter');
        }

        $post_desc .= '<br>'.sprintf( esc_html__( 'If you have enabled %sEnable auto posting to Twitter%s in global settings then you do not need to check this box to publish/schedule the post. This setting is only for republishing Or rescheduling post to Twitter.', 'wpwautoposter'), '<strong>', '</strong>');
        $post_desc .= '<br><p class="wpw-auto-poster-meta wpw-auto-poster-meta_second"><strong>'.esc_html__('Note:', 'wpwautoposter').'</strong> '. sprintf( esc_html__( 'This setting is just an event to republish/reschedule the content, It will not save any value to %sdatabase%s.', 'wpwautoposter'), '<strong>','</strong>').'</p>';

        //post to twitter
        $poster_meta->addPublishBox($prefix . 'post_to_twitter', array('name' => $post_label, 'desc' => $post_desc, 'tab' => 'wpw_twitter'));

        //Immediate post to twitter
        if( !empty($schedule_option)) {
            $poster_meta->addPublishBox($prefix . 'immediate_post_to_twitter', array('name' => esc_html__('Immediate Posting On Twitter:', 'wpwautoposter'), 'desc' => 'Immediately publish this post to Twitter.', 'tab' => 'wpw_twitter'));
        }

        //post to this account 
        $poster_meta->addSelect($prefix . 'tw_user_id', $tw_users, array('name' => esc_html__('Post To This Twitter Account', 'wpwautoposter') . '(' . esc_html__('s', 'wpwautoposter') . '):', 'std' => array(''), 'desc' => esc_html__('Select an account to which you want to post. This setting overrides the global and category settings. Leave it  empty to use the global/category defaults.', 'wpwautoposter'), 'multiple' => true, 'placeholder' => esc_html__('Default', 'wpwautoposter'), 'tab' => 'wpw_twitter'));

        //tweet mode
        $poster_meta->addTweetMode($prefix . 'tw_tweet_mode', array('name' => esc_html__('Mode:', 'wpwautoposter'), 'desc' => esc_html__('Tweet Template Mode.', 'wpwautoposter'), 'tab' => 'wpw_twitter'));
        
        if( empty($wpw_auto_poster_options['tw_disable_image_tweet']) ) {
            //tweet image url
            $poster_meta->addImage($prefix . 'tw_image', array('name' => esc_html__('Tweet Image:', 'wpwautoposter'), 'desc' => esc_html__('Here you can upload a default image which will be used for the Tweet Image. Leave it empty to use the featured image. if featured image is also blank, then it will take default image from the settings page.', 'wpwautoposter'), 'tab' => 'wpw_twitter', 'show_path' => true));
        }
        
        //tweet template, do not change the order for tweet template and tweet preview field
        $poster_meta->addTweetTemplate($prefix . 'tw_template', array('default' => $defaulttemplate, 'validate_func' => 'escape_html', 'name' => esc_html__('Tweet Template:', 'wpwautoposter'), 'desc' => esc_html__('Here you can enter a custom Tweeter template. Leave it empty to use the default one from the settings page. You can use following template tags within the status text:', 'wpwautoposter') .
            '<br /><code>{title}</code> - ' . esc_html__('displays the post title,', 'wpwautoposter') .
            '<br /><code>{link}</code> - ' . esc_html__('displays the post link,', 'wpwautoposter') .
            '<br /><code>{full_author}</code> - ' . esc_html__('displays the full author name,', 'wpwautoposter') .
            '<br /><code>{nickname_author}</code> - ' . esc_html__('displays the nickname of author,', 'wpwautoposter') .
            '<br /><code>{post_type}</code> - ' . esc_html__('displays the post type,', 'wpwautoposter') .
            '<br /><code>{excerpt}</code> - ' . esc_html__('displays the post excerpt.', 'wpwautoposter').
            '<br /><code>{hashtags}</code> - ' . esc_html__('displays the post tags as hashtags.', 'wpwautoposter').
            '<br /><code>{hashcats}</code> - ' . esc_html__('displays the post categories as hashtags.', 'wpwautoposter').
            '<br /><code>{content}</code> - ' . esc_html__('displays the post content.', 'wpwautoposter').
            '<br /><code>{content-digits}</code> - ' . sprintf(
                esc_html__('displays the post content with define number of digits in template tag. %s E.g. If you add template like {content-100} then it will display first 100 characters from post content.', 'wpwautoposter'),
                "<b>",
                "</b>"
            ).
            '<br /><code>{CF-CustomFieldName}</code> - ' . sprintf(
                esc_html__('inserts the contents of the custom field with the specified name. %s E.g. If your price is stored in the custom field "PRDPRICE" you will need to use {CF-PRDPRICE} tag.', 'wpwautoposter'),
                "<b>", "</b>"
            )
            ,'tab' => 'wpw_twitter'));

        //add label to show preview, do not change the order for tweet template and tweet preview field
        $poster_meta->addTweetPreview($prefix . 'tw_template', array('default' => $defaulttemplate, 'validate_func' => 'escape_html', 'name' => esc_html__('Preview:', 'wpwautoposter'), 'tab' => 'wpw_twitter'));
    }
    /*     * *********************************** Twitter Tab Ends ***************************************************** */

    /*     * *********************************** LinkedIn Tab Starts ***************************************************** */
    if (!isset($wpw_auto_poster_options['prevent_post_li_metabox']) || empty($wpw_auto_poster_options['prevent_post_li_metabox'])) { //check if not allowed for individual post in settings page
        $limetatab = array(
            'class' => 'wpw_linkedin', //unique class name of each tabs
            'title' => esc_html__('LinkedIn', 'wpwautoposter'), //  title of tab
            'active' => $defaulttabon //it will by default make tab active on page load
        );

        $defaulttabon = false; //when linkedin is on then inactive other tab by default
        //initiate tabs in metabox
        $poster_meta->addTabs($limetatab);

        // Get stored li app grant data
        $wpw_auto_poster_li_sess_data = get_option('wpw_auto_poster_li_sess_data');

        if (!is_ssl()) {

            $poster_meta->addGrantPermission($prefix . 'li_warning', array('desc' => esc_html__('Linkedin requires SSL for posting.', 'wpwautoposter'), 'url' => '', 'urltext' =>'', 'tab' => 'wpw_linkedin'));
        }
        elseif (WPW_AUTO_POSTER_LI_APP_ID == '' || WPW_AUTO_POSTER_LI_APP_SECRET == '') {

            $poster_meta->addGrantPermission($prefix . 'li_warning', array('desc' => esc_html__('Enter your LinkedIn Application Details within the Settings Page, otherwise posting to LinkedIn won\'t work.', 'wpwautoposter'), 'url' => add_query_arg(array('page' => 'wpw-auto-poster-settings'), admin_url('admin.php')), 'urltext' => esc_html__('Go to the Settings Page', 'wpwautoposter'), 'tab' => 'wpw_linkedin'));

        } elseif( empty( $wpw_auto_poster_li_sess_data ) ) {

            $poster_meta->addGrantPermission($prefix . 'li_grant', array('desc' => esc_html__('Your App doesn\'t have enough permissions to publish on Linkedin.', 'wpwautoposter'), 'url' => add_query_arg(array('page' => 'wpw-auto-poster-settings'), admin_url('admin.php')), 'urltext' => esc_html__('Grant extended permissions now', 'wpwautoposter'), 'tab' => 'wpw_linkedin'));
        }

        //add label to show status
        $poster_meta->addTweetStatus($prefix . 'li_status', array('name' => esc_html__('Status:', 'wpwautoposter'), 'desc' => esc_html__('Status of LinkedIn wall post like published/unpublished/scheduled.', 'wpwautoposter'), 'tab' => 'wpw_linkedin'));

        $post_status = get_post_meta($post_id, $prefix.'li_status', true );
        $post_label  = esc_html__('Publish Post On LinkedIn:', 'wpwautoposter');
        $post_desc   = esc_html__('Publish this Post to your LinkedIn.', 'wpwautoposter');

        if( $post_status == 1 && empty($schedule_option)) {
            $post_label = esc_html__('Re-publish Post On LinkedIn:', 'wpwautoposter');
            $post_desc  = esc_html__('Re-publish this Post to your LinkedIn.', 'wpwautoposter');
        } elseif ( ( $post_status == 2 ) || ( $post_status == 1 && !empty($schedule_option) ) ) {
            $post_label = esc_html__('Re-schedule Post On LinkedIn:', 'wpwautoposter');
            $post_desc  = esc_html__('Re-schedule this Post to your LinkedIn.', 'wpwautoposter');
        } elseif ( empty($post_status) && !empty($schedule_option) ) {
            $post_label  = esc_html__('Schedule Post On LinkedIn:', 'wpwautoposter');
            $post_desc   = esc_html__('Schedule this Post to your LinkedIn.', 'wpwautoposter');
        }

        $post_desc .= '<br>'.sprintf( esc_html__( 'If you have enabled %sEnable auto posting to LinkedIn%s in global settings then you do not need to check this box to publish/schedule the post. This setting is only for republishing Or rescheduling post to LinkedIn.', 'wpwautoposter'), '<strong>', '</strong>');
        $post_desc .= '<br><p class="wpw-auto-poster-meta wpw-auto-poster-meta_second"><strong>'.esc_html__('Note:', 'wpwautoposter').'</strong> '. sprintf( esc_html__( 'This setting is just an event to republish/reschedule the content, It will not save any value to %sdatabase%s.', 'wpwautoposter'), '<strong>','</strong>').'</p>';

        //post to linkedin
        $poster_meta->addPublishBox($prefix . 'post_to_linkedin', array('name' => $post_label, 'desc' => $post_desc, 'tab' => 'wpw_linkedin'));

        //Immediate post to LinkedIn
        if( !empty($schedule_option)) {
            $poster_meta->addPublishBox($prefix . 'immediate_post_to_linkedin', array('name' => esc_html__('Immediate Posting On LinkedIn:', 'wpwautoposter'), 'desc' => 'Immediately publish this post to LinkedIn.', 'tab' => 'wpw_linkedin'));
        }

        //publish status to linkedin
        $poster_meta->addTextarea($prefix . 'li_post_title', array('validate_func' => 'escape_html', 'name' => esc_html__('Custom Title:', 'wpwautoposter'), 'desc' => esc_html__('Here you can enter a custom title which will be used for the wall post. Leave it empty to use the post title. You can use following template tags within the custom title:', 'wpwautoposter').
            '<br /><code>{first_name}</code> - ' . esc_html__('displays the first name,', 'wpwautoposter') .
            '<br /><code>{last_name}</code> - ' . esc_html__('displays the last name,', 'wpwautoposter') .
            '<br /><code>{title}</code> - ' . esc_html__('displays the default post title,', 'wpwautoposter') .
            '<br /><code>{link}</code> - ' . esc_html__('displays the default post link,', 'wpwautoposter') .
            '<br /><code>{full_author}</code> - ' . esc_html__('displays the full author name,', 'wpwautoposter') .
            '<br /><code>{nickname_author}</code> - ' . esc_html__('displays the nickname of author,', 'wpwautoposter') .
            '<br /><code>{post_type}</code> - ' . esc_html__('displays the post type,', 'wpwautoposter') .
            '<br /><code>{sitename}</code> - ' . esc_html__('displays the name of your site,', 'wpwautoposter') .
            '<br /><code>{excerpt}</code> - ' . esc_html__('displays the post excerpt.', 'wpwautoposter').
            '<br /><code>{hashtags}</code> - ' . esc_html__('displays the post tags as hashtags.', 'wpwautoposter').
            '<br /><code>{hashcats}</code> - ' . esc_html__('displays the post categories as hashtags.', 'wpwautoposter').
            '<br /><code>{content}</code> - ' . esc_html__('displays the post content.', 'wpwautoposter').
            '<br /><code>{content-digits}</code> - ' . sprintf(
                esc_html__('displays the post content with define number of digits in template tag. %s E.g. If you add template like {content-100} then it will display first 100 characters from post content.', 'wpwautoposter'),
                "<b>", "</b>"
            ).
            '<br /><code>{CF-CustomFieldName}</code> - ' . sprintf(
                esc_html__('inserts the contents of the custom field with the specified name. %s E.g. If your price is stored in the custom field "PRDPRICE" you will need to use {CF-PRDPRICE} tag.%s', 'wpwautoposter'),
                "<b>", "</b>"
            )
            , 'tab' => 'wpw_linkedin', 'rows' => 3 ));

        $li_profiles = $liposting->wpw_auto_poster_get_profiles_data();

        //post to this account
        $poster_meta->addSelect($prefix . 'li_post_profile', $li_profiles, array('name' => esc_html__('Post To This Linkedin Account', 'wpwautoposter') . '(' . esc_html__('s', 'wpwautoposter') . '):', 'std' => array(''), 'desc' => esc_html__('Select an account to which you want to post. This setting overrides the global and category settings. Leave it  empty to use the global/category defaults.', 'wpwautoposter'), 'multiple' => true, 'placeholder' => esc_html__('Default', 'wpwautoposter'), 'tab' => 'wpw_linkedin'));

        //publish status to linkedin image
        $poster_meta->addImage($prefix . 'li_post_image', array('name' => esc_html__('Post Image:', 'wpwautoposter'), 'desc' => esc_html__('Here you can upload a default image which will be used for the LinkedIn wall post. Leave it empty to use the featured image. if featured image is also blank, then it will take default image from the settings page.', 'wpwautoposter'), 'tab' => 'wpw_linkedin', 'show_path' => true));

        //custom link to post to facebook
        $poster_meta->addText($prefix . 'li_post_link', array('validate_func' => 'escape_html', 'name' => esc_html__('Custom Link:', 'wpwautoposter'), 'desc' => esc_html__('Here you can enter a custom link which will be used for the wall post. Leave it empty to use the link of the current post. The link must start with', 'wpwautoposter') . ' http://', 'tab' => 'wpw_linkedin'));

        //comment to linkedin
        $poster_meta->addTextarea($prefix . 'li_post_comment', array('validate_func' => 'escape_html', 'name' => esc_html__('Custom Message:', 'wpwautoposter'), 'desc' => esc_html__('Here you can customize the content which will be used by LinkedIn for the wall post. You can use following template tags within the status text:', 'wpwautoposter') .
            '<br /><code>{first_name}</code> - ' . esc_html__('displays the first name,', 'wpwautoposter') .
            '<br /><code>{last_name}</code> - ' . esc_html__('displays the last name,', 'wpwautoposter') .
            '<br /><code>{title}</code> - ' . esc_html__('displays the post title,', 'wpwautoposter') .
            '<br /><code>{post_content}</code> - ' . esc_html__('displays the post content,', 'wpwautoposter') .
            '<br /><code>{link}</code> - ' . esc_html__('displays the post link,', 'wpwautoposter') .
            '<br /><code>{full_author}</code> - ' . esc_html__('displays the full author name,', 'wpwautoposter') .
            '<br /><code>{nickname_author}</code> - ' . esc_html__('displays the nickname of author,', 'wpwautoposter') .
            '<br /><code>{post_type}</code> - ' . esc_html__('displays the post type,', 'wpwautoposter') .
            '<br /><code>{sitename}</code> - ' . esc_html__('displays the name of your site.', 'wpwautoposter') .
            '<br /><code>{excerpt}</code> - ' . esc_html__('displays the post excerpt.', 'wpwautoposter').
            '<br /><code>{hashtags}</code> - ' . esc_html__('displays the post tags as hashtags.', 'wpwautoposter').
            '<br /><code>{hashcats}</code> - ' . esc_html__('displays the post categories as hashtags.', 'wpwautoposter').
            '<br /><code>{content}</code> - ' . esc_html__('displays the post content.', 'wpwautoposter').
            '<br /><code>{content-digits}</code> - ' . sprintf(
                esc_html__('displays the post content with define number of digits in template tag. %s E.g. If you add template like {content-100} then it will display first 100 characters from post content.%s', 'wpwautoposter'),
                "<b>", "</b>"
            ).
            '<br /><code>{CF-CustomFieldName}</code> - ' . sprintf(
                esc_html__('inserts the contents of the custom field with the specified name. %s E.g. If your price is stored in the custom field "PRDPRICE" you will need to use {CF-PRDPRICE} tag.%s', 'wpwautoposter'), "<b>", "</b>"
            ) 
            , 'tab' => 'wpw_linkedin'));


    }
    /*     * *********************************** LinkedIn Tab Ends   ***************************************************** */

    /*     * *********************************** Tumblr Tab Starts ***************************************************** */
    if (!isset($wpw_auto_poster_options['prevent_post_tb_metabox']) || empty($wpw_auto_poster_options['prevent_post_tb_metabox'])) { //check if not allowed for individual post in settings page
        $tbmetatab = array(
            'class' => 'wpw_tumblr', //unique class name of each tabs
            'title' => esc_html__('Tumblr', 'wpwautoposter'), //  title of tab
            'active' => $defaulttabon //it will by default make tab active on page load
        );

        //Posting type
        $tb_posting_types = array(
            '' => esc_html__('Select', 'wpwautoposter'),
            'text'  => esc_html__('Text', 'wpwautoposter'),
            'link'  => esc_html__('Link', 'wpwautoposter'),
            'photo' => esc_html__('Photo', 'wpwautoposter')
        );

        $defaulttabon = false; //when tumblr is on then inactive other tab by default
        //initiate tabs in metabox
        $poster_meta->addTabs($tbmetatab);

        // Get stored tb app grant data
        $wpw_auto_poster_tb_sess_data = get_option('wpw_auto_poster_tb_sess_data');

        $tb_users = wpw_auto_poster_get_tb_accounts();

        if (WPW_AUTO_POSTER_TB_CONS_KEY == '' || WPW_AUTO_POSTER_TB_CONS_SECRET == '') {

            $poster_meta->addGrantPermission($prefix . 'tb_warning', array('desc' => esc_html__('Enter your Tumblr Application Details within the Settings page, otherwise the posting to Tumblr won\'t work.', 'wpwautoposter'), 'url' => add_query_arg(array('page' => 'wpw-auto-poster-settings'), admin_url('admin.php')), 'urltext' => esc_html__('Go to the Settings Page', 'wpwautoposter'), 'tab' => 'wpw_tumblr'));

        } elseif ( empty( $wpw_auto_poster_tb_sess_data ) ) {

            $poster_meta->addGrantPermission($prefix . 'tb_grant', array('desc' => esc_html__('Your App doesn\'t have enough permissions to publish on Tumblr.', 'wpwautoposter'), 'url' => $tbposting->wpw_auto_poster_get_tb_login_url(), 'urltext' => esc_html__('Grant extended permissions now.', 'wpwautoposter'), 'tab' => 'wpw_tumblr'));
        }

        //add label to show status
        $poster_meta->addTweetStatus($prefix . 'tb_status', array('name' => esc_html__('Status:', 'wpwautoposter'), 'desc' => esc_html__('Status of Tumblr wall post like published/unpublished/scheduled.', 'wpwautoposter'), 'tab' => 'wpw_tumblr'));

        $post_status = get_post_meta($post_id, $prefix.'tb_status', true );
        $post_label  = esc_html__('Publish Post On Tumblr:', 'wpwautoposter');
        $post_desc   = esc_html__('Publish this Post to Tumblr Userwall.', 'wpwautoposter');

        if( $post_status == 1 && empty($schedule_option)) {
            $post_label = esc_html__('Re-publish Post On Tumblr:', 'wpwautoposter');
            $post_desc  = esc_html__('Re-publish this Post to Tumblr Userwall.', 'wpwautoposter');
        } elseif ( ( $post_status == 2 ) || ( $post_status == 1 && !empty($schedule_option) ) ) {
            $post_label = esc_html__('Re-schedule Post On Tumblr:', 'wpwautoposter');
            $post_desc  = esc_html__('Re-schedule this Post to Tumblr Userwall.', 'wpwautoposter');
        } elseif ( empty($post_status) && !empty($schedule_option) ) {
            $post_label  = esc_html__('Schedule Post On Tumblr:', 'wpwautoposter');
            $post_desc   = esc_html__('Schedule this Post to Tumblr Userwall.', 'wpwautoposter');
        }

        $post_desc .= '<br>'.sprintf( esc_html__( 'If you have enabled %sEnable auto posting to Tumblr%s in global settings then you do not need to check this box to publish/schedule the post. This setting is only for republishing Or rescheduling post to Tumblr.', 'wpwautoposter'), '<strong>', '</strong>');
        $post_desc .= '<br><p class="wpw-auto-poster-meta wpw-auto-poster-meta_second"><strong>'.esc_html__('Note:', 'wpwautoposter').'</strong> '. sprintf( esc_html__( 'This setting is just an event to republish/reschedule the content, It will not save any value to %sdatabase%s.', 'wpwautoposter'), '<strong>','</strong>').'</p>';

        //post to tumblr
        $poster_meta->addPublishBox($prefix . 'post_to_tumblr', array('name' => $post_label, 'desc' => $post_desc, 'tab' => 'wpw_tumblr'));

        //Immediate post to tumblr
        if( !empty($schedule_option)) {
            $poster_meta->addPublishBox($prefix . 'immediate_post_to_tumblr', array('name' => esc_html__('Immediate Posting On Tumblr:', 'wpwautoposter'), 'desc' => 'Immediately publish this post to Tumblr.', 'tab' => 'wpw_tumblr'));
        }

        //posting type
        $poster_meta->addSelect($prefix . 'tb_posting_type', $tb_posting_types, array('name' => esc_html__('Posting Type:', 'wpwautoposter'), 'std' => array(''), 'desc' => esc_html__('Choose posting type which you want to use. Leave it empty to use the default one from the settings page.', 'wpwautoposter'), 'tab' => 'wpw_tumblr'));

        //publish status to tumblr
        $poster_meta->addTextarea($prefix . 'tb_post_title', array('validate_func' => 'escape_html', 'name' => esc_html__('Custom Title:', 'wpwautoposter'), 'desc' => esc_html__('Here you can enter a custom title which will be used on the wall post. Leave it empty to use the post title. You can use following template tags within the custom title:', 'wpwautoposter').
            '<br /><code>{first_name}</code> - ' . esc_html__('displays the first name,', 'wpwautoposter') .
            '<br /><code>{last_name}</code> - ' . esc_html__('displays the last name,', 'wpwautoposter') .
            '<br /><code>{title}</code> - ' . esc_html__('displays the default post title,', 'wpwautoposter') .
            '<br /><code>{link}</code> - ' . esc_html__('displays the default post link,', 'wpwautoposter') .
            '<br /><code>{full_author}</code> - ' . esc_html__('displays the full author name,', 'wpwautoposter') .
            '<br /><code>{nickname_author}</code> - ' . esc_html__('displays the nickname of author,', 'wpwautoposter') .
            '<br /><code>{post_type}</code> - ' . esc_html__('displays the post type,', 'wpwautoposter') .
            '<br /><code>{sitename}</code> - ' . esc_html__('displays the name of your site,', 'wpwautoposter') .
            '<br /><code>{excerpt}</code> - ' . esc_html__('displays the post excerpt.', 'wpwautoposter').
            '<br /><code>{hashtags}</code> - ' . esc_html__('displays the post tags as hashtags.', 'wpwautoposter').
            '<br /><code>{hashcats}</code> - ' . esc_html__('displays the post categories as hashtags.', 'wpwautoposter').
            '<br /><code>{content}</code> - ' . esc_html__('displays the post content.', 'wpwautoposter').
            '<br /><code>{content-digits}</code> - ' . sprintf(
                esc_html__('displays the post content with define number of digits in template tag. %s E.g. If you add template like {content-100} then it will display first 100 characters from post content.%s', 'wpwautoposter'),
                "<b>", "</b>"
            ).
            '<br /><code>{CF-CustomFieldName}</code> - ' . sprintf(
                esc_html__('inserts the contents of the custom field with the specified name. %s E.g. If your price is stored in the custom field "PRDPRICE" you will need to use {CF-PRDPRICE} tag.%s', 'wpwautoposter'),
                "<b>","</b>"
            )
            , 'tab' => 'wpw_tumblr', 'rows' => '3' ));

        $poster_meta->addSelect($prefix . 'tb_user_id', $tb_users, array('name' => esc_html__('Post To This Tumblr Account', 'wpwautoposter') . '(' . esc_html__('s', 'wpwautoposter') . '):', 'std' => array(''), 'desc' => esc_html__('Select an account to which you want to post. This setting overrides the global settings. Leave it  empty to use the global defaults.', 'wpwautoposter'), 'multiple' => true, 'placeholder' => esc_html__('Default', 'wpwautoposter'), 'tab' => 'wpw_tumblr'));

        //post link
        $poster_meta->addText($prefix . 'tb_custom_post_link', array('validate_func' => 'escape_html', 'name' => esc_html__('Custom Link:', 'wpwautoposter'), 'desc' => esc_html__('Here you can enter a custom link which will be used on the wall post. Leave it empty to use the link to the post. The link must start with http://', 'wpwautoposter'), 'tab' => 'wpw_tumblr'));

        //publish status descriptin to tumblr
        $poster_meta->addTextarea($prefix . 'tb_post_desc', array('validate_func' => 'escape_html', 'name' => esc_html__('Custom Message:', 'wpwautoposter'), 'desc' => esc_html__('Here you can enter custom content which will appear underneath the post title in Tumblr. Leave it empty to use the post content. You can use following template tags within the custom message:', 'wpwautoposter') .
            '<br /><code>{first_name}</code> - ' . esc_html__('display the first name,', 'wpwautoposter') .
            '<br /><code>{last_name}</code> - ' . esc_html__('display the last name,', 'wpwautoposter') .
            '<br /><code>{title}</code> - ' . esc_html__('display the post title,', 'wpwautoposter') .
            '<br /><code>{full_author}</code> - ' . esc_html__('displays the full author name,', 'wpwautoposter') .
            '<br /><code>{nickname_author}</code> - ' . esc_html__('displays the nickname of author,', 'wpwautoposter') .
            '<br /><code>{post_type}</code> - ' . esc_html__('displays the post type,', 'wpwautoposter') .
            '<br /><code>{sitename}</code> - ' . esc_html__('display the sitename/blogname.', 'wpwautoposter').
            '<br /><code>{hashtags}</code> - ' . esc_html__('displays the post tags as hashtags.', 'wpwautoposter').
            '<br /><code>{hashcats}</code> - ' . esc_html__('displays the post categories as hashtags.', 'wpwautoposter').
            '<br /><code>{content}</code> - ' . esc_html__('displays the post content.', 'wpwautoposter').
            '<br /><code>{content-digits}</code> - ' . sprintf(
                esc_html__('displays the post content with define number of digits in template tag. %s E.g. If you add template like {content-100} then it will display first 100 characters from post content.%s', 'wpwautoposter'), "<b>", "</b>"
            ).
            '<br /><code>{CF-CustomFieldName}</code> - ' . sprintf(
                esc_html__('inserts the contents of the custom field with the specified name. %s E.g. If your price is stored in the custom field "PRDPRICE" you will need to use {CF-PRDPRICE} tag.%s', 'wpwautoposter'), "<b>", "</b>"
            )
            , 'tab' => 'wpw_tumblr'));

        //post image url
        $poster_meta->addImage($prefix . 'tb_post_image', array('name' => esc_html__('Post Image:', 'wpwautoposter'), 'desc' => esc_html__('Here you can upload a default image which will be used for the Tumblr post. Leave it empty to use the featured image. if featured image is also blank, then it will take default image from the settings page.', 'wpwautoposter'), 'tab' => 'wpw_tumblr', 'show_path' => true));
    }
    /*     * *********************************** Tumblr Tab Ends ***************************************************** */


    /** 
     * Youtube Tab Starts
     * @since 2.6.0
    **/
    if (!isset($wpw_auto_poster_options['prevent_post_yt_metabox']) || empty($wpw_auto_poster_options['prevent_post_yt_metabox'])) { //check if not allowed for individual post in settings page
        $ytmetatab = array(
                'class' => 'wpw_youtube', //unique class name of each tabs
                'title' => esc_html__('Youtube', 'wpwautoposter'), //  title of tab
                'active' => $defaulttabon //it will by default make tab active on page load
            );

            $defaulttabon = false; //when youtube is on then inactive other tab by default
            //initiate tabs in metabox
            $poster_meta->addTabs($ytmetatab);

            // Get stored fb app grant data
            $wpw_auto_poster_yt_sess_data = get_option('wpw_auto_poster_yt_sess_data');
            $yt_keys = $wpw_auto_poster_options['yt_keys'];
            $yt_users = array();

            if(!empty($wpw_auto_poster_yt_sess_data)){
                foreach ($wpw_auto_poster_yt_sess_data as $key => $yt_account) {
                    $yt_users[$yt_account['wpw_auto_poster_yt_cache']['id']] = trim($yt_account['wpw_auto_poster_yt_cache']['id']); 
                }
            }

            if (empty($yt_keys) || count($yt_keys) < 1) {

                $poster_meta->addGrantPermission($prefix . 'yt_warning', array('desc' => esc_html__('Enter your Youtube Application details within the Settings Page, otherwise posting to Youtube won\'t work.', 'wpwautoposter'), 'url' => add_query_arg(array('page' => 'wpw-auto-poster-settings'), admin_url('admin.php')), 'urltext' => esc_html__('Go to the Settings Page', 'wpwautoposter'), 'tab' => 'wpw_youtube'));
            } 
            elseif (empty($wpw_auto_poster_yt_sess_data)) { // Check youtube set or not
                $poster_meta->addGrantPermission($prefix . 'yt_grant', array('desc' => esc_html__('Your App doesn\'t have enough permissions to publish on Youtube.', 'wpwautoposter'), 'url' => add_query_arg(array('page' => 'wpw-auto-poster-settings'), admin_url('admin.php')), 'urltext' => esc_html__('Go to the Settings Page', 'wpwautoposter'), 'tab' => 'wpw_youtube'));
            }

            //add label to show status
            $poster_meta->addTweetStatus($prefix . 'yt_published_on_yt', array('name' => esc_html__('Status:', 'wpwautoposter'), 'desc' => esc_html__('Status of Youtube post like published/unpublished/scheduled.', 'wpwautoposter'), 'tab' => 'wpw_youtube'));

            $post_status = get_post_meta($post_id, $prefix.'yt_published_on_yt', true );

            $post_label  = esc_html__('Publish Post On Youtube:', 'wpwautoposter');
            $post_desc   = esc_html__('Publish this Post to Youtube timeline.', 'wpwautoposter');

            if( $post_status == 1 && empty($schedule_option)) {
                $post_label = esc_html__('Re-publish Post On Youtube:', 'wpwautoposter');
                $post_desc  = esc_html__('Re-publish this Post to Youtube timeline.', 'wpwautoposter');
            } elseif ( ( $post_status == 2 ) || ( $post_status == 1 && !empty($schedule_option) ) ) {
                $post_label = esc_html__('Re-schedule Post On Youtube:', 'wpwautoposter');
                $post_desc  = esc_html__('Re-schedule this Post to Youtube timeline.', 'wpwautoposter');
            } elseif ( empty($post_status) && !empty($schedule_option) ) {
                $post_label  = esc_html__('Schedule Post On Youtube:', 'wpwautoposter');
                $post_desc   = esc_html__('Schedule this Post to Youtube timeline.', 'wpwautoposter');
            }

            $post_desc .= '<br>'.sprintf( esc_html__( 'If you have enabled %sEnable auto posting to Youtube%s in global settings then you do not need to check this box to publish/schedule the post. This setting is only for republishing Or rescheduling post to Youtube.', 'wpwautoposter'), '<strong>', '</strong>');
            $post_desc .= '<br><p style="color:#c83737;" classs="wpw-auto-poster-meta"><strong>'.esc_html__('Note:', 'wpwautoposter').'</strong> '. sprintf( esc_html__( 'This setting is just an event to republish/reschedule the content, It will not save any value to %sdatabase%s.', 'wpwautoposter'), '<strong>','</strong>').'</p>';

            //post to youtube
            $poster_meta->addPublishBox($prefix . 'post_to_youtube', array('name' => $post_label, 'desc' => $post_desc, 'tab' => 'wpw_youtube'));

            //Immediate post to youtube
            if( !empty($schedule_option)) {
                $poster_meta->addPublishBox($prefix . 'immediate_post_to_youtube', array('name' => esc_html__('Immediate Posting On Youtube:', 'wpwautoposter'), 'desc' => 'Immediately publish this post to Youtube.', 'tab' => 'wpw_youtube'));
            }


            //post to this account
            $poster_meta->addSelect($prefix . 'yt_user_id', $yt_users, array('name' => esc_html__('Post To This Youtube Account', 'wpwautoposter') . '(' . esc_html__('s', 'wpwautoposter') . '):', 'std' => array(''), 'desc' => esc_html__('Select an account to which you want to post. This setting overrides the global settings. Leave it  empty to use the global defaults.', 'wpwautoposter'), 'multiple' => true, 'placeholder' => esc_html__('Default', 'wpwautoposter'), 'tab' => 'wpw_youtube'));

            $poster_meta->addTextarea($prefix . 'yt_post_title', array('validate_func' => 'escape_html', 'name' => esc_html__('Custom Title:', 'wpwautoposter'), 'desc' => esc_html__('Here you can enter a custom title which will be used for the YouTube video title. Leave it empty to use the post title. You can use following template tags within the custom title:', 'wpwautoposter').
                '<br /><code>{first_name}</code> - ' . esc_html__('displays the first name,', 'wpwautoposter') .
                '<br /><code>{last_name}</code> - ' . esc_html__('displays the last name,', 'wpwautoposter') .
                '<br /><code>{title}</code> - ' . esc_html__('displays the default post title,', 'wpwautoposter') .
                '<br /><code>{link}</code> - ' . esc_html__('displays the default post link,', 'wpwautoposter') .
                '<br /><code>{full_author}</code> - ' . esc_html__('displays the full author name,', 'wpwautoposter') .
                '<br /><code>{nickname_author}</code> - ' . esc_html__('displays the nickname of author,', 'wpwautoposter') .
                '<br /><code>{post_type}</code> - ' . esc_html__('displays the post type,', 'wpwautoposter') .
                '<br /><code>{sitename}</code> - ' . esc_html__('displays the name of your site,', 'wpwautoposter') .
                '<br /><code>{excerpt}</code> - ' . esc_html__('displays the post excerpt.', 'wpwautoposter').
                '<br /><code>{hashtags}</code> - ' . esc_html__('displays the post tags as hashtags.', 'wpwautoposter').
                '<br /><code>{hashcats}</code> - ' . esc_html__('displays the post categories as hashtags.', 'wpwautoposter').
                '<br /><code>{content}</code> - ' . esc_html__('displays the post content.', 'wpwautoposter').
                '<br /><code>{content-digits}</code> - ' . sprintf(
                    esc_html__('displays the post content with define number of digits in template tag. %s E.g. If you add template like {content-100} then it will display first 100 characters from post content. %s', 'wpwautoposter'),
                    "<b>", "</b>"
                ).
                '<br /><code>{CF-CustomFieldName}</code> - ' . sprintf(
                    esc_html__('inserts the contents of the custom field with the specified name. %s E.g. If your price is stored in the custom field "PRDPRICE" you will need to use {CF-PRDPRICE} tag.%s', 'wpwautoposter'),
                    "<b>", "</b>"
                )
                , 'tab' => 'wpw_youtube', 'rows' => 3 ));

            //post image 
            $poster_meta->addVideo($prefix . 'yt_post_image', array('name' => esc_html__('Post Video:', 'wpwautoposter'), 'desc' => esc_html__('Here you can upload a default video which will be used for the Youtube post. Leave it empty to use the global video from the settings page.', 'wpwautoposter'), 'tab' => 'wpw_youtube', 'show_path' => true));

            //post message
            $poster_meta->addTextarea($prefix . 'yt_custom_status_msg', array('default' => '', 'validate_func' => 'escape_html', 'name' => esc_html__('Custom Message:', 'wpwautoposter'), 'desc' => esc_html__('Here you can enter a custom caption text. Leave it empty to  use the default one from the settings page. You can use following template tags within the caption text:', 'wpwautoposter') .
                '<br /><code>{first_name}</code> - ' . esc_html__('displays the first name,', 'wpwautoposter') .
                '<br /><code>{last_name}</code> - ' . esc_html__('displays the last name,', 'wpwautoposter') .
                '<br /><code>{display_name}</code> - ' . esc_html__('displays the display name,', 'wpwautoposter') .
                '<br /><code>{title}</code> - ' . esc_html__('displays the post title,', 'wpwautoposter') .
                '<br /><code>{excerpt}</code> - ' . esc_html__('displays the short post description,', 'wpwautoposter') .
                '<br /><code>{link}</code> - ' . esc_html__('displays the post link,', 'wpwautoposter') .
                '<br /><code>{full_author}</code> - ' . esc_html__('displays the full author name,', 'wpwautoposter') .
                '<br /><code>{nickname_author}</code> - ' . esc_html__('displays the nickname of author,', 'wpwautoposter') .
                '<br /><code>{post_type}</code> - ' . esc_html__('displays the post type,', 'wpwautoposter') .
                '<br /><code>{sitename}</code> - ' . esc_html__('displays the name of your site.', 'wpwautoposter').
                '<br /><code>{hashtags}</code> - ' . esc_html__('displays the post tags as hashtags.', 'wpwautoposter').
                '<br /><code>{hashcats}</code> - ' . esc_html__('displays the post categories as hashtags.', 'wpwautoposter').
                '<br /><code>{content-digits}</code> - ' .sprintf(esc_html__('displays the post content %s  %s{content-digits}%s - displays the post content with define number of digits in template tag. %s E.g. If you add template like {content-100} then it will display first 100 characters from post content. %s', 'wpwautoposter'),"<br />","<code>","</code>", "<b>", "</b>"
            ).
                '<br /><code>{CF-CustomFieldName}</code> - ' .sprintf(esc_html__('inserts the contents of the custom field with the specified name.  %s E.g. If your price is stored in the custom field "PRDPRICE" you will need to use {CF-PRDPRICE} tag. %s', 'wpwautoposter'), "<b>", "</b>"
            ), 'tab' => 'wpw_youtube'));
        }
        /** *********************************** Youtube Tab Ends **************************************************** **/


        /** 
     * Pinterest Tab Starts
     * @since 2.6.0
    **/
    if (!isset($wpw_auto_poster_options['prevent_post_pin_metabox']) || empty($wpw_auto_poster_options['prevent_post_pin_metabox'])) { //check if not allowed for individual post in settings page
        $pinmetatab = array(
            'class' => 'wpw_pinterest', //unique class name of each tabs
            'title' => esc_html__('Pinterest', 'wpwautoposter'), //  title of tab
            'active' => $defaulttabon //it will by default make tab active on page load
        );

        $defaulttabon = false; //when pinterest is on then inactive other tab by default
        //initiate tabs in metabox
        $poster_meta->addTabs($pinmetatab);

        // Get stored pin app grant data
        $wpw_auto_poster_pin_sess_data = get_option('wpw_auto_poster_pin_sess_data');

        // Get all pinterest account authenticated
        $pin_users = wpw_auto_poster_get_pin_accounts('all_accounts');

        if (!is_ssl()) {

            $poster_meta->addGrantPermission($prefix . 'pin_warning', array('desc' => esc_html__('Pinterest requires SSL for posting to boards.', 'wpwautoposter'), 'url' => '', 'urltext' =>'', 'tab' => 'wpw_pinterest'));
        } elseif (WPW_AUTO_POSTER_PIN_APP_ID == '' || WPW_AUTO_POSTER_PIN_APP_SECRET == '') { // Check pinterest application id and secret must entered in settings page or not

            $poster_meta->addGrantPermission($prefix . 'pin_warning', array('desc' => esc_html__('Enter your Pinterest APP ID / Secret within the Settings Page, otherwise the Pinterest posting won\'t work.', 'wpwautoposter'), 'url' => add_query_arg(array('page' => 'wpw-auto-poster-settings'), admin_url('admin.php')), 'urltext' => esc_html__('Go to the Settings Page', 'wpwautoposter'), 'tab' => 'wpw_pinterest'));
        } elseif (empty($wpw_auto_poster_pin_sess_data)) { // Check pinterest user id is set or not
            $poster_meta->addGrantPermission($prefix . 'pin_grant', array('desc' => esc_html__('Your App doesn\'t have enough permissions to publish on Pinterest.', 'wpwautoposter'), 'url' => add_query_arg(array('page' => 'wpw-auto-poster-settings'), admin_url('admin.php')), 'urltext' => esc_html__('Go to the Settings Page', 'wpwautoposter'), 'tab' => 'wpw_pinterest'));
        }

        //add label to show status
        $poster_meta->addTweetStatus($prefix . 'pin_published_on_pin', array('name' => esc_html__('Status:', 'wpwautoposter'), 'desc' => esc_html__('Status of Pinterest board post like published/unpublished/scheduled.', 'wpwautoposter'), 'tab' => 'wpw_pinterest'));

        $post_status = get_post_meta($post_id, $prefix.'pin_published_on_pin', true );
        $post_label  = esc_html__('Publish Post On Pinterest:', 'wpwautoposter');
        $post_desc   = esc_html__('Publish this Post to Pinterest board.', 'wpwautoposter');

        if( $post_status == 1 && empty($schedule_option)) {
            $post_label = esc_html__('Re-publish Post On Pinterest:', 'wpwautoposter');
            $post_desc  = esc_html__('Re-publish this Post to Pinterest board.', 'wpwautoposter');
        } elseif ( ( $post_status == 2 ) || ( $post_status == 1 && !empty($schedule_option) ) ) {
            $post_label = esc_html__('Re-schedule Post On Pinterest:', 'wpwautoposter');
            $post_desc  = esc_html__('Re-schedule this Post to Pinterest board.', 'wpwautoposter');
        } elseif ( empty($post_status) && !empty($schedule_option) ) {
            $post_label  = esc_html__('Schedule Post On Pinterest:', 'wpwautoposter');
            $post_desc   = esc_html__('Schedule this Post to Pinterest board.', 'wpwautoposter');
        }

        $post_desc .= '<br>'.sprintf( esc_html__( 'If you have enabled %sEnable auto posting to Pinterest%s in global settings then you do not need to check this box to publish/schedule the post. This setting is only for republishing Or rescheduling post to Pinterest.', 'wpwautoposter'), '<strong>', '</strong>');
        $post_desc .= '<br><p classs="wpw-auto-poster-meta wpw-auto-poster-meta_second"><strong>'.esc_html__('Note:', 'wpwautoposter').'</strong> '. sprintf( esc_html__( 'This setting is just an event to republish/reschedule the content, It will not save any value to %sdatabase%s.', 'wpwautoposter'), '<strong>','</strong>').'</p>';

        //post to pinterest
        $poster_meta->addPublishBox($prefix . 'post_to_pinterest', array('name' => $post_label, 'desc' => $post_desc, 'tab' => 'wpw_pinterest'));

        //Immediate post to pinterest
        if( !empty($schedule_option)) {
            $poster_meta->addPublishBox($prefix . 'immediate_post_to_pinterest', array('name' => esc_html__('Immediate Posting On Pinterest:', 'wpwautoposter'), 'desc' => 'Immediately publish this post to Pinterest.', 'tab' => 'wpw_pinterest'));
        }

        //post to this account
        $poster_meta->addSelect($prefix . 'pin_user_id', $pin_users, array('name' => esc_html__('Post To This Pinterest Account', 'wpwautoposter') . '(' . esc_html__('s', 'wpwautoposter') . '):', 'std' => array(''), 'desc' => esc_html__('Select an account to which you want to post. This setting overrides the global settings. Leave it  empty to use the global defaults.', 'wpwautoposter'), 'multiple' => true, 'placeholder' => esc_html__('Default', 'wpwautoposter'), 'tab' => 'wpw_pinterest'));

        //custom link to post to pinterest
        $poster_meta->addText($prefix . 'pin_custom_post_link', array('validate_func' => 'escape_html', 'name' => esc_html__('Custom Link:', 'wpwautoposter'), 'desc' => esc_html__('Here you can enter a custom link which will be used for  the board pins. Leave it empty to use the link of the current post.', 'wpwautoposter'), 'tab' => 'wpw_pinterest'));

        $poster_meta->addImage($prefix . 'pin_post_image', array('name' => esc_html__('Post Image:', 'wpwautoposter'), 'desc' => esc_html__('Here you can upload a default image which will be used for the Pinterest post. Leave it empty to use the featured image. if featured image is also blank, then it will take default image from the settings page.', 'wpwautoposter').'<br><br><strong>'.esc_html__('Note:', 'wpwautoposter').' </strong>'.esc_html__('You need to select atleast one image, otherwise pinterest posting will not work.', 'wpwautoposter'), 'tab' => 'wpw_pinterest', 'show_path' => true));

        //publish with diffrent post title
        $poster_meta->addTextarea($prefix . 'pin_custom_status_msg', array('default' => '', 'validate_func' => 'escape_html', 'name' => esc_html__('Custom Message:', 'wpwautoposter'), 'desc' => esc_html__('Here you can enter a custom note text. Leave it empty to  use the default one from the settings page. You can use following template tags within the notes text:', 'wpwautoposter') .
            '<br /><code>{first_name}</code> - ' . esc_html__('displays the first name,', 'wpwautoposter') .
            '<br /><code>{last_name}</code> - ' . esc_html__('displays the last name,', 'wpwautoposter') .
            '<br /><code>{title}</code> - ' . esc_html__('displays the post title,', 'wpwautoposter') .
            '<br /><code>{excerpt}</code> - ' . esc_html__('displays the short post description,', 'wpwautoposter') .
            '<br /><code>{link}</code> - ' . esc_html__('displays the post link,', 'wpwautoposter') .
            '<br /><code>{full_author}</code> - ' . esc_html__('displays the full author name,', 'wpwautoposter') .
            '<br /><code>{nickname_author}</code> - ' . esc_html__('displays the nickname of author,', 'wpwautoposter') .
            '<br /><code>{post_type}</code> - ' . esc_html__('displays the post type,', 'wpwautoposter') .
            '<br /><code>{sitename}</code> - ' . esc_html__('displays the name of your site,', 'wpwautoposter').'<br /><code>{hashtags}</code> - ' . esc_html__('displays the post tags as hashtags,', 'wpwautoposter').
            '<br /><code>{hashcats}</code> - ' . esc_html__('displays the post categories as hashtags,', 'wpwautoposter').
            '<br /><code>{content}</code> - ' . esc_html__('displays the post content,', 'wpwautoposter').
            '<br /><code>{content-digits}</code> - ' . sprintf(
                esc_html__('displays the post content with define number of digits in template tag, %s E.g. If you add template like {content-100} then it will display first 100 characters from post content. %s', 'wpwautoposter'), "<b>", "</b>"
            ).
            '<br /><code>{CF-CustomFieldName}</code> - ' . sprintf(
                esc_html__('inserts the contents of the custom field with the specified name. %s E.g. If your price is stored in the custom field "PRDPRICE" you will need to use {CF-PRDPRICE} tag.%s', 'wpwautoposter'), "<b>", "</b>"
            )
            , 'tab' => 'wpw_pinterest'));
        
    }
    /** *********************************** Pinterest Tab Ends **************************************************** **/


    /** *********************************** Google My Business Tab **************************************************** **/
    /** 
     * Google My Business Tab Starts
     * @since 2.6.0
    **/
    /*if (!isset($wpw_auto_poster_options['prevent_post_gmb_metabox']) || empty($wpw_auto_poster_options['prevent_post_gmb_metabox'])) { //check if not allowed for individual post in settings page
        $gmbmetatab = array(
            'class' => 'wpw_gmb', //unique class name of each tabs
            'title' => esc_html__('Google My Business', 'wpwautoposter'), //  title of tab
            'active' => $defaulttabon //it will by default make tab active on page load
        );

        $defaulttabon = false; //when Google My Business is on then inactive other tab by default
        //initiate tabs in metabox
        $poster_meta->addTabs($gmbmetatab);

        // Get stored Google My Business app grant data
        $wpw_auto_poster_gmb_sess_data = get_option('wpw_auto_poster_gmb_sess_data');

        // Get all Google My Business account authenticated
        $gmb_users = wpw_auto_poster_get_gmb_accounts_location();

        //add label to show status
        $poster_meta->addTweetStatus($prefix . 'gmb_published_on_posts', array('name' => esc_html__('Status:', 'wpwautoposter'), 'desc' => esc_html__('Status of Google My Business board post like published/unpublished/scheduled.', 'wpwautoposter'), 'tab' => 'wpw_gmb'));

        $post_status = get_post_meta($post_id, $prefix.'gmb_published_on_posts', true );
        $post_label  = esc_html__('Publish Post On Google My Business:', 'wpwautoposter');
        $post_desc   = esc_html__('Publish this Post to Google My Business.', 'wpwautoposter');

        if( $post_status == 1 && empty($schedule_option)) {
            $post_label = esc_html__('Re-publish Post On Google My Business:', 'wpwautoposter');
            $post_desc  = esc_html__('Re-publish this Post to Google My Business.', 'wpwautoposter');
        } elseif ( ( $post_status == 2 ) || ( $post_status == 1 && !empty($schedule_option) ) ) {
            $post_label = esc_html__('Re-schedule Post On Google My Business:', 'wpwautoposter');
            $post_desc  = esc_html__('Re-schedule this Post to Google My Business.', 'wpwautoposter');
        } elseif ( empty($post_status) && !empty($schedule_option) ) {
            $post_label  = esc_html__('Schedule Post On Google My Business:', 'wpwautoposter');
            $post_desc   = esc_html__('Schedule this Post to Google My Business.', 'wpwautoposter');
        }

        $post_desc .= '<br>'.sprintf( esc_html__( 'If you have enabled %sEnable auto posting to Google My Business%s in global settings then you do not need to check this box to publish/schedule the post. This setting is only for republishing Or rescheduling post to Google My Business.', 'wpwautoposter'), '<strong>', '</strong>');
        $post_desc .= '<br><p classs="wpw-auto-poster-meta wpw-auto-poster-meta_second"><strong>'.esc_html__('Note:', 'wpwautoposter').'</strong> '. sprintf( esc_html__( 'This setting is just an event to republish/reschedule the content, It will not save any value to %sdatabase%s.', 'wpwautoposter'), '<strong>','</strong>').'</p>';

        //post to Google My Business
        $poster_meta->addPublishBox($prefix . 'post_to_gmb', array('name' => $post_label, 'desc' => $post_desc, 'tab' => 'wpw_gmb'));

        //Immediate post to Google My Business
        if( !empty($schedule_option)) {
            $poster_meta->addPublishBox($prefix . 'immediate_post_to_gmb', array('name' => esc_html__('Immediate Posting On Google My Business:', 'wpwautoposter'), 'desc' => 'Immediately publish this post to Google My Business.', 'tab' => 'wpw_gmb'));
        }


        //post to this account
            $poster_meta->addSelect($prefix . 'gmb_user_id', $gmb_users, array('name' => esc_html__('Post To This Google My Business Account', 'wpwautoposter') . '(' . esc_html__('s', 'wpwautoposter') . '):', 'std' => array(''), 'desc' => esc_html__('Select an account to which you want to post. This setting overrides the global settings. Leave it  empty to use the global defaults.', 'wpwautoposter'), 'multiple' => true, 'placeholder' => esc_html__('Default', 'wpwautoposter'), 'tab' => 'wpw_gmb'));

        

            $poster_meta->addImage($prefix . 'gmb_post_image', array('name' => esc_html__('Post Image:', 'wpwautoposter'), 'desc' => esc_html__('Here you can upload a default image which will be used for the Google My Business post. Leave it empty to use the featured image. if featured image is also blank, then it will take default image from the settings page.', 'wpwautoposter').'<br><br><strong>'.esc_html__('Note:', 'wpwautoposter').' </strong>'.esc_html__('You need to select atleast one image, otherwise pinterest posting will not work.', 'wpwautoposter'), 'tab' => 'wpw_gmb', 'show_path' => true));

        //publish with diffrent post title
            $poster_meta->addTextarea($prefix . 'gmb_custom_status_msg', array('default' => '', 'validate_func' => 'escape_html', 'name' => esc_html__('Custom Message:', 'wpwautoposter'), 'desc' => esc_html__('Here you can enter a custom note text. Leave it empty to  use the default one from the settings page. You can use following template tags within the notes text:', 'wpwautoposter') .
                '<br /><code>{first_name}</code> - ' . esc_html__('displays the first name,', 'wpwautoposter') .
                '<br /><code>{last_name}</code> - ' . esc_html__('displays the last name,', 'wpwautoposter') .
                '<br /><code>{title}</code> - ' . esc_html__('displays the post title,', 'wpwautoposter') .
                '<br /><code>{excerpt}</code> - ' . esc_html__('displays the short post description,', 'wpwautoposter') .
                '<br /><code>{full_author}</code> - ' . esc_html__('displays the full author name,', 'wpwautoposter') .
                '<br /><code>{nickname_author}</code> - ' . esc_html__('displays the nickname of author,', 'wpwautoposter') .
                '<br /><code>{post_type}</code> - ' . esc_html__('displays the post type,', 'wpwautoposter') .
                '<br /><code>{sitename}</code> - ' . esc_html__('displays the name of your site,', 'wpwautoposter').'<br /><code>{hashtags}</code> - ' . esc_html__('displays the post tags as hashtags,', 'wpwautoposter').
                '<br /><code>{hashcats}</code> - ' . esc_html__('displays the post categories as hashtags,', 'wpwautoposter').
                '<br /><code>{content}</code> - ' . esc_html__('displays the post content,', 'wpwautoposter').
                '<br /><code>{content-digits}</code> - ' . sprintf(
                    esc_html__('displays the post content with define number of digits in template tag, %s E.g. If you add template like {content-100} then it will display first 100 characters from post content. %s', 'wpwautoposter'), "<b>", "</b>"
                ).
                '<br /><code>{CF-CustomFieldName}</code> - ' . sprintf(
                    esc_html__('inserts the contents of the custom field with the specified name. %s E.g. If your price is stored in the custom field "PRDPRICE" you will need to use {CF-PRDPRICE} tag.%s', 'wpwautoposter'), "<b>", "</b>"
                )
                , 'tab' => 'wpw_gmb'));
            
        }*/
        /** *********************************** Google My Business Tab Ends **************************************************** **/

        

    /** 
     * Instagram Tab Starts
     * @since 2.6.0
    **/

    do_action( 'wpw_auto_poster_tab_add_meta_boxes_after_ba', $poster_meta );

    /*     * *********************************** Instagram Tab Ends ***************************************************** */


    /*     * *********************************** BufferApp Tab Starts ***************************************************** */
    if (!isset($wpw_auto_poster_options['prevent_post_ba_metabox']) || empty($wpw_auto_poster_options['prevent_post_ba_metabox'])) { //check if not allowed for individual post in settings page
        $dcmetatab = array(
            'class' => 'wpw_bufferapp', //unique class name of each tabs
            'title' => esc_html__('BufferApp', 'wpwautoposter'), //  title of tab
            'active' => $defaulttabon //it will by default make tab active on page load
        );

        $defaulttabon = false; //when bufferapp is on then inactive other tab by default
        //initiate tabs in metabox
        $poster_meta->addTabs($dcmetatab);

        //get all bufferapp account authenticated
        $ba_users = array();

        $ba_users[''] = esc_html__('--Select--', 'wpwautoposter');

        if (isset($_SESSION['wpw_auto_poster_ba_cache']) && !empty($_SESSION['wpw_auto_poster_ba_cache'])) {

            foreach ($_SESSION['wpw_auto_poster_ba_cache'] as $key => $account) {

                $ba_users[$account->id] = $account->formatted_username;
            }
        }

        if (WPW_AUTO_POSTER_BA_CLIENT_ID == '' || WPW_AUTO_POSTER_BA_CLIENT_SECRET == '') {

            $poster_meta->addGrantPermission($prefix . 'ba_warning', array('desc' => esc_html__('Enter your BufferApp Application Details within the Settings Page, otherwise posting to BufferApp won\'t work.', 'wpwautoposter'), 'url' => admin_url('edit.php?post_type=wpw_auto_poster&page=wpw-auto-poster-settings'), 'urltext' => esc_html__('Go to the Settings Page', 'wpwautoposter'), 'tab' => 'wpw_bufferapp'));
        } else {
            if (!isset($_SESSION['wpw_auto_poster_ba_user_id']) || empty($_SESSION['wpw_auto_poster_ba_user_id'])) {
                $poster_meta->addGrantPermission($prefix . 'tb_grant', array('desc' => esc_html__('Your App doesn\'t have enough permissions to publish on BufferApp.', 'wpwautoposter'), 'url' => admin_url('edit.php?post_type=wpw_auto_poster&page=wpw-auto-poster-settings'), 'urltext' => esc_html__('Go to Settings Page', 'wpwautoposter'), 'tab' => 'wpw_bufferapp'));
            }
        }

        //add label to show status
        $poster_meta->addTweetStatus($prefix . 'ba_status', array('name' => esc_html__('Status:', 'wpwautoposter'), 'desc' => esc_html__('Status of BufferApp wall post like published/unpublished/scheduled.', 'wpwautoposter'), 'tab' => 'wpw_bufferapp'));

        $post_status = get_post_meta($post_id, $prefix.'ba_status', true );
        $post_label  = esc_html__('Publish Post On BufferApp:', 'wpwautoposter');
        $post_desc   = esc_html__('Publish this post to your BufferApp Userwall.', 'wpwautoposter');

        if( $post_status == 1 && empty($schedule_option)) {
            $post_label = esc_html__('Re-publish Post On BufferApp:', 'wpwautoposter');
            $post_desc  = esc_html__('Re-publish this post to your BufferApp Userwall.', 'wpwautoposter');
        } elseif ( ( $post_status == 2 ) || ( $post_status == 1 && !empty($schedule_option) ) ) {
            $post_label = esc_html__('Re-schedule Post On BufferApp:', 'wpwautoposter');
            $post_desc  = esc_html__('Re-schedule this post to your BufferApp Userwall.', 'wpwautoposter');
        } elseif ( empty($post_status) && !empty($schedule_option) ) {
            $post_label  = esc_html__('Schedule Post On BufferApp:', 'wpwautoposter');
            $post_desc   = esc_html__('Schedule this Post to your BufferApp Userwall.', 'wpwautoposter');
        }

        $post_desc .= '<br>'.sprintf( esc_html__( 'If you have enabled %sEnable auto posting to BufferApp%s in global settings then you do not need to check this box to publish/schedule the post. This setting is only for republishing Or rescheduling post to BufferApp.', 'wpwautoposter'), '<strong>', '</strong>');
        $post_desc .= '<br><p classs="wpw-auto-poster-meta wpw-auto-poster-meta_second"><strong>'.esc_html__('Note:', 'wpwautoposter').'</strong> '. sprintf( esc_html__( 'This setting is just an event to republish/reschedule the content, It will not save any value to %sdatabase%s.', 'wpwautoposter'), '<strong>','</strong>').'</p>';

        //post to bufferapp
        $poster_meta->addPublishBox($prefix . 'post_to_bufferapp', array('name' => $post_label, 'desc' => $post_desc, 'tab' => 'wpw_bufferapp'));

        //Immediate post to BufferApp
        if( !empty($schedule_option)) {
            $poster_meta->addPublishBox($prefix . 'immediate_post_to_bufferapp', array('name' => esc_html__('Immediate Posting On BufferApp:', 'wpwautoposter'), 'desc' => 'Immediately publish this post to BufferApp.', 'tab' => 'wpw_bufferapp'));
        }

        //publish status to bufferapp
        $poster_meta->addTextarea($prefix . 'ba_post_title', array('validate_func' => 'escape_html', 'name' => esc_html__('Custom Title:', 'wpwautoposter'), 'desc' => esc_html__('Here you can enter a custom title which will be used for the wall post. Leave it empty to use the post title. You can use following template tags within the custom title:', 'wpwautoposter').
            '<br /><code>{first_name}</code> - ' . esc_html__('displays the first name,', 'wpwautoposter') .
            '<br /><code>{last_name}</code> - ' . esc_html__('displays the last name,', 'wpwautoposter') .
            '<br /><code>{title}</code> - ' . esc_html__('displays the default post title,', 'wpwautoposter') .
            '<br /><code>{full_author}</code> - ' . esc_html__('displays the full author name,', 'wpwautoposter') .
            '<br /><code>{nickname_author}</code> - ' . esc_html__('displays the nickname of author,', 'wpwautoposter') .
            '<br /><code>{post_type}</code> - ' . esc_html__('displays the post type,', 'wpwautoposter') .
            '<br /><code>{sitename}</code> - ' . esc_html__('displays the name of your site,', 'wpwautoposter') .
            '<br /><code>{excerpt}</code> - ' . esc_html__('displays the post excerpt.', 'wpwautoposter').
            '<br /><code>{hashtags}</code> - ' . esc_html__('displays the post tags as hashtags.', 'wpwautoposter').
            '<br /><code>{hashcats}</code> - ' . esc_html__('displays the post categories as hashtags.', 'wpwautoposter').
            '<br /><code>{content}</code> - ' . esc_html__('displays the post content.', 'wpwautoposter').
            '<br /><code>{content-digits}</code> - ' . sprintf(
                esc_html__('displays the post content with define number of digits in template tag. %s E.g. If you add template like {content-100} then it will display first 100 characters from post content.%s', 'wpwautoposter'), "<b>", "</b>"
            ).
            '<br /><code>{CF-CustomFieldName}</code> - ' . sprintf(
                esc_html__('inserts the contents of the custom field with the specified name. %s E.g. If your price is stored in the custom field "PRDPRICE" you will need to use {CF-PRDPRICE} tag.%s', 'wpwautoposter'), "<b>", "</b>"
            )
            , 'tab' => 'wpw_bufferapp', 'rows' => '3' ));

        //post to this account
        $poster_meta->addSelect($prefix . 'ba_post_to_accounts', $ba_users, array('name' => esc_html__('Publish To This BufferApp Account:', 'wpwautoposter'), 'std' => array(''), 'desc' => esc_html__('Select an account to which you want to post. Leave it empty to use the default one from the settings page.', 'wpwautoposter'), 'multiple' => true, 'placeholder' => esc_html__('Default', 'wpwautoposter'), 'tab' => 'wpw_bufferapp'));

        //publish status to bufferapp image
        $poster_meta->addImage($prefix . 'ba_post_image', array('name' => esc_html__('Post Image:', 'wpwautoposter'), 'desc' => esc_html__('Here you can upload a default image which will be used for the BufferApp wall post. Leave it empty to use the featured image. if featured image is also blank, then it will take default image from the settings page.', 'wpwautoposter'), 'tab' => 'wpw_bufferapp', 'show_path' => true));

        //custom link to post to facebook
        $poster_meta->addText($prefix . 'ba_custom_post_link', array('validate_func' => 'escape_html', 'name' => esc_html__('Custom Link:', 'wpwautoposter'), 'desc' => esc_html__('Here you can enter a custom link which will be used for the wall post. Leave it empty to use the link of the current post. The link must start with', 'wpwautoposter') . ' http://<br /><strong>' . esc_html__('Note', 'wpwautoposter') . ' : </strong>' . esc_html__('Link is only used for posting on facebook profile(s).', 'wpwautoposter'), 'tab' => 'wpw_bufferapp'));

    }
    /*     * *********************************** BufferApp Tab Ends ***************************************************** */
    
    if ($defaulttabon) { // Check no active tab
        //meta settings are not available
        $poster_meta->addParagraph($prefix . 'no_meta_settings', array('value' => esc_html__('There is no meta settings allowed to be set for individual posts from global setting.', 'wpwautoposter')));
    }

    /*
     * Don't Forget to Close up the meta box decleration
     */
    //Finish Meta Box Decleration

    $poster_meta->Finish();
}

// add action to add custom meta box in custom post
add_action('load-post.php', 'wpw_auto_poster_add_meta_boxes');
add_action('load-post-new.php', 'wpw_auto_poster_add_meta_boxes');